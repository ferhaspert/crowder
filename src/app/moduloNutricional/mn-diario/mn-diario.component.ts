import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MnCrearPlanComponent } from '../mn-crear-plan/mn-crear-plan.component';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { ModalErrorComponent } from 'app/modals/modal-error/modal-error.component';
import { UtilsService } from 'app/services/utils.service';
import { MessageService } from 'app/services/message.service';

export class DatosPlan {
  usuarioId: number;
  alimentos: number[];
  cantidades: number[];
  cumplido: boolean;
  menuId: number;
}

@Component({
  selector: 'app-mn-diario',
  templateUrl: './mn-diario.component.html',
  styleUrls: ['./mn-diario.component.scss'],
})
export class MnDiarioComponent implements OnInit {

  @Output() cambiarATab = new EventEmitter<number>();

  public tipoNuevoPlan: number;
  public planActual: any;
  public menuesDelDia: any;
  public datosPlan: DatosPlan;
  public dataLoaded;
  public user;
  public subscription;

  constructor(
    public dialog: MatDialog,
    private httpService: HttpService,
    private routeService: RoutesService,
    private modalService: ModalService,
    private utils: UtilsService,
    private messageService: MessageService,
  ) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        if (message === 'reload') {
          this.ngOnInit();
        }
      }
    });
  }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.httpService.realizarGet(
      this.routeService.moduloNutricional + '/nutrischool/gestionNutricional/getDiarioNutricional?clienteId=' + this.user.id)
      .subscribe(resultado => {
        // TODO abrir modal en caso de que no tenga planes activos
        this.planActual = resultado;
        this.dataLoaded = true;
        this.planActual.menuesPlanDia.sort((a, b) => a.id - b.id);
        this.planActual.menuesPlanDia.forEach(menu => {
          menu.confirmado = false;
          menu.real = false;
        });
        this.actualizarMenuesReales();
      });
  }

  actualizarMenuesReales() {
    if (this.planActual.menuesRealesDia.length > 0) {
      this.planActual.menuesRealesDia.forEach(menuReal => {
        this.planActual.menuesPlanDia.forEach(menuDia => {
          if (menuReal.nombre === menuDia.nombre) {
            menuDia.alimentos = [];
            menuReal.alimentosReales.forEach(alimento => {
              menuDia.alimentos.push({alimento: alimento.alimentoReal, cantidad: alimento.cantidad});
            });
            menuDia.confirmado = true;
            menuDia.real = true;
          }
        });
      });
    }
  }

  redireccionarAPestana(tabChange: number): void {
    this.cambiarATab.emit(tabChange);
  }

  crearNuevoPlan(): void {
    const dialogRef = this.dialog.open(MnCrearPlanComponent, {
      width: '350px',
      data: 'mn',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.tipoNuevoPlan = result;
      this.redireccionarAPestana(1);
    });
  }

  modalError() {
    this.modalService.openModalError("No se pudo dar de alta el plan, por favor intente más tarde.")
  }

}
