import { Component, OnInit, Inject } from '@angular/core';
import { MnDiaCardComponent } from '../mn-dia-card/mn-dia-card.component';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, switchMap } from 'rxjs/operators';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';

@Component({
  selector: 'app-mn-agregar-alimento',
  templateUrl: './mn-agregar-alimento.component.html',
  styleUrls: ['./mn-agregar-alimento.component.scss'],
})
export class MnAgregarAlimentoComponent implements OnInit {

  public alimento: any = {alimento: {}, cantidad: 0};
  alimentoForm = new FormControl('', Validators.required);
  cantidadForm = new FormControl('', [Validators.pattern('^[1-9]+$'), Validators.required]);
  public invalidAlimentoForm;
  public invalidCantidadForm;

  options: any[] = [
    // {"id": 1, "descripcion": "Fideos"},
    // {"id": 2, "descripcion": "Pure"}
  ];

  filteredOptions: Observable<any[]>;

  constructor(
    public dialogRef: MatDialogRef<MnDiaCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public httpService: HttpService,
    public routeService: RoutesService) {}

  onNoClick(): void {
    this.alimento = undefined;
    this.dialogRef.close();
  }

  ngOnInit() {
    this.filteredOptions = this.alimentoForm
      .valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        switchMap(value => {
          return this._filter(value);
        }),
      );
  }

  public agregar() {
    this.invalidCantidadForm = this.invalidAlimentoForm = false;
    if (this.alimentoForm.value !== this.alimento.alimento.nombre) {
      this.invalidAlimentoForm = true;
    }

    if (this.cantidadForm.value === '' || this.cantidadForm.invalid) {
      this.invalidCantidadForm = true;
    }

    if (this.invalidAlimentoForm || this.invalidCantidadForm) {
      return;
    }

    this.alimento.cantidad = this.cantidadForm.value;
    this.dialogRef.close(this.alimento);
  }

  public selectAlimento(alimento) {
    this.alimento.alimento = alimento;
  }

  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    const data: any = {};
    data.options = {
      nombreAlimento: filterValue
    };
    return this.httpService.listarAlimentosSinLoader(filterValue);
  }

}
