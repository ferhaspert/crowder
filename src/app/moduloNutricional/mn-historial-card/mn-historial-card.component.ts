import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DetallePlanNutricionalComponent } from 'app/modals/detalle-plan-nutricional/detalle-plan-nutricional.component';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { UtilsService } from 'app/services/utils.service';
import { MessageService } from 'app/services/message.service';
import { Router, RouterModule } from '@angular/router';
import { MnMiPlanComponent } from '../mn-mi-plan/mn-mi-plan.component';

@Component({
  selector: 'app-mn-historial-card',
  templateUrl: './mn-historial-card.component.html',
  styleUrls: ['./mn-historial-card.component.scss'],
})
export class MnHistorialCardComponent implements OnInit {

  public planes : any[];
  public user;
  public subscription;

  constructor(
    public dialog: MatDialog,
    private httpService: HttpService, 
    private routeService: RoutesService,
    private modalService: ModalService,
    private utils: UtilsService,
    private messageService: MessageService,
    private router: Router,
  ) {
  this.subscription = this.messageService.getMessage().subscribe(message => {
          if (message) {
            if (message === 'reload') {
              this.ngOnInit();
            }
          }
        });
  
  }

  ngOnInit() {
    this.planes = [];
    this.user = this.utils.loadFromLocalStorage('user');
    this.httpService.realizarGet(
      this.routeService.moduloNutricional + "/nutrischool/gestionNutricional/listarPlanesUsuario?clienteId=" + this.user.id)
      .subscribe(resultado => {
        this.planes = resultado;
        this.planes.forEach(element => {
          element.planNutricional.dias.sort((a, b) => b.id - a.id);
          for (let dia of element.planNutricional.dias) {
            dia.menues.sort((a, b) => a.id - b.id);
          }
        });

        console.log(this.planes);
        // TODO abrir modal en caso de que no tenga planes activos
      }); 
  }

  esCliente(){
    return (this.user.rol == 2);
  }

  estadoPlan(plan){
    return plan.estadoPlan == 1 ? "Activo" : "No Activo";
  }
  
  verDetalle(plan): void {
    const dialogRef = this.dialog.open(DetallePlanNutricionalComponent, {
      data: plan,
      panelClass: 'modal-detalle-plan'
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      });
  }

  eliminar(plan){
    let clienteId = this.user.id;
    let planId = plan.planNutricional.id;
    this.httpService.realizarPost(this.routeService.moduloNutricional + '/nutrischool/gestionNutricional/cambioEstadoPlanUsuario?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=3' ,{})
      .subscribe(response => {
        if (response.result === 0) {      
          this.modalService.openModalSuccess(response.message,{});
          this.messageService.sendMessage('reload');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
  }

  desactivar(plan){
    let clienteId = this.user.id;
    let planId = plan.planNutricional.id;
    if(plan.estadoPlan == 2){
      this.modalService.openModalInfo('El plan ya se encuentra inactivo.');
    }else{
      this.httpService.realizarPost(this.routeService.moduloNutricional + '/nutrischool/gestionNutricional/cambioEstadoPlanUsuario?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=2' ,{})
      .subscribe(response => {
        if (response.result === 0) {      
          this.messageService.sendMessage('reload');
          this.modalService.openModalSuccess(response.message,{});
        } else {
          this.modalService.openModalError(response.message);
        }
      });
    }
  }

  reload() {
    this.router.navigateByUrl('mn/tabs');
  }

  activar(plan){
    let clienteId = this.user.id;
    let planId = plan.planNutricional.id;
    if(plan.estadoPlan == 1){
      this.modalService.openModalInfo('El plan ya se encuentra activo.');
    }else{
      this.httpService.realizarPost(this.routeService.moduloNutricional + '/nutrischool/gestionNutricional/cambioEstadoPlanUsuario?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=1' ,{})
      .subscribe(response => {
        if (response.result === 0) { 
          this.messageService.sendMessage('reload');     
          this.modalService.openModalSuccess(response.message, {});
        } else {
          this.modalService.openModalError(response.message);
        }
      });
    }
  }
}
