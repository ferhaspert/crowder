import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UtilsService } from 'app/services/utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mn-tabs',
  templateUrl: './mn-tabs.component.html',
  styleUrls: ['./mn-tabs.component.scss'],
})
export class MnTabsComponent implements OnInit {

  public selected = new FormControl(0);
  public user;

  constructor(
    private utils: UtilsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.selected.setValue(0);
    this.user = this.utils.loadFromLocalStorage('user');
    if (this.user.condiciones.length === 0) {
      this.router.navigateByUrl('condicionesIniciales/nutricionales');
    }
  }

  crearNuevoPlanSelected(tab: number) {
    this.selected.setValue(tab);
  }
}
