import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MnAgregarAlimentoComponent } from 'app/moduloNutricional/mn-agregar-alimento/mn-agregar-alimento.component';
import { HttpService } from 'app/services/http.service';
import { Dia } from 'app/interfaces/moduloNutricional/interfaces';

@Component({
  selector: 'app-mn-dia-card',
  templateUrl: './mn-dia-card.component.html',
  styleUrls: ['./mn-dia-card.component.scss'],
})
export class MnDiaCardComponent implements OnInit {

  @Input() dia: Dia;

  constructor(
    public dialog: MatDialog,
    public http: HttpService
  ) { }

  ngOnInit() {
    console.log(this.dia);
  }

  agregarAlimento(): void {
    const dialogRef = this.dialog.open(MnAgregarAlimentoComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
