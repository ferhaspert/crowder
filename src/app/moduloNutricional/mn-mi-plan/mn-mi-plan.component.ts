import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { PlanActivo } from '../../interfaces/moduloNutricional/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { MnCrearPlanComponent } from '../../moduloNutricional/mn-crear-plan/mn-crear-plan.component';
import { UtilsService } from '../../services/utils.service';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'app-mn-mi-plan',
  templateUrl: './mn-mi-plan.component.html',
  styleUrls: ['./mn-mi-plan.component.scss'],
})
export class MnMiPlanComponent implements OnInit {

  public planActivo: PlanActivo;
  public user;
  public dataLoaded;
  public subscription;

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    private utils: UtilsService,
    private messageService: MessageService,
  ) {
  this.subscription = this.messageService.getMessage().subscribe(message => {
          if (message) {
            if (message === 'reload') {
              this.ngOnInit();
            }
          }
        });
  }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.http.listarPlanActivoUsuario(this.user.id)
      .subscribe(data => {
        this.dataLoaded = true;
        if (data === null) {
          this.planActivo = undefined;
          return;
        }
        this.planActivo = data;
        this.planActivo.planNutricional.dias.sort((a, b) => a.id - b.id);
        for (let dia of this.planActivo.planNutricional.dias) {
          dia.menues.sort((a, b) => a.id - b.id);
        }
      });
  }

  crearNuevoPlan() {
    const dialogRef = this.dialog.open(MnCrearPlanComponent, {
      width: '350px',
      data: 'mn'
    });
  }
}
