import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpService } from 'app/services/http.service';
import { MnAgregarAlimentoComponent } from '../mn-agregar-alimento/mn-agregar-alimento.component';
import { RoutesService } from 'app/services/routes.service';
import { DatosPlan } from '../mn-diario/mn-diario.component';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-mn-comida-card',
  templateUrl: './mn-comida-card.component.html',
  styleUrls: ['./mn-comida-card.component.scss'],
})
export class MnComidaCardComponent implements OnInit {

  private cumplido: boolean = true;

  @Input() menu: any;

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    public routeService: RoutesService,
    public modalService: ModalService,
  ) { }

  ngOnInit() {}

  agregarAlimento(): void {
    const dialogRef = this.dialog.open(MnAgregarAlimentoComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(alimento => {
      if(alimento != undefined || alimento.cantidad != undefined || alimento.alimento != undefined) {
        this.menu.alimentos.push(alimento);
        this.cumplido = false;
      }
    });
  }

  armarEnvio() {
    // var data = {"cantidades": [], "alimentos": [], "cumplido": this.cumplido, };
    let data = new DatosPlan();
    // data = this.datosPlan;
    data.cantidades = [];
    data.alimentos = [];
    data.cumplido = this.cumplido;
    data.menuId = this.menu.id;
    data.usuarioId = JSON.parse(localStorage.getItem('user')).id;
    this.menu.alimentos.forEach(element => {
      data.cantidades.push(element.cantidad);
      data.alimentos.push(element.alimento.id);
    });
    return data;
  }

  confirmarMenu() {
    var data = this.armarEnvio();
    this.http.realizarPost(this.routeService.moduloNutricional + "/nutrischool/gestionNutricional/registrarProgresoPlan", data)
      .subscribe(response => {
        if(response.result == 0 || response.result == 4){
          this.menu.confirmado = true;
        }else{
          this.modalService.openModalError("No se ha podido informar el menú.");
        }
      })
  }

  editarMenu() {
    this.menu.confirmado = false;
  }

  borrarAlimento(index: number) {
    this.menu.alimentos.splice(index, 1);
    this.cumplido = false;
  }

}
