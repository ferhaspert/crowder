import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MnCrearPlanVentanaComponent } from './mn-crear-plan-ventana.component';

describe('MnCrearPlanVentanaComponent', () => {
  let component: MnCrearPlanVentanaComponent;
  let fixture: ComponentFixture<MnCrearPlanVentanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MnCrearPlanVentanaComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MnCrearPlanVentanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
