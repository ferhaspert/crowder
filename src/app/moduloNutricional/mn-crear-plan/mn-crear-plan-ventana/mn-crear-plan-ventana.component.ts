import { Component, OnInit } from '@angular/core';
import { PlanActivo, Dia, Menu, Alimento, PlanNutricional, MenuNuevo } from 'app/interfaces/moduloNutricional/interfaces';
import { MnAgregarAlimentoComponent } from 'app/moduloNutricional/mn-agregar-alimento/mn-agregar-alimento.component';
import { MatDialog } from '@angular/material/dialog';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'app/services/utils.service';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'app-mn-crear-plan-ventana',
  templateUrl: './mn-crear-plan-ventana.component.html',
  styleUrls: ['./mn-crear-plan-ventana.component.scss'],
})
export class MnCrearPlanVentanaComponent implements OnInit {

  public plan: PlanActivo;
  public user;
  public dietaNombre;
  private dias = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  private menus = ['Desayuno', 'Almuerzo', 'Cena', 'Merienda', 'Colación'];

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    public routesService: RoutesService,
    private router: Router,
    public alertController: AlertController,
    private utils: UtilsService,
    public messageService: MessageService,
  ) {}

  ngOnInit() {
    this.plan = this.crearPlanVacio();
    console.log(this.plan);
  }

  crearPlanVacio() {
    let plan = new PlanActivo();
    plan.planNutricional = new PlanNutricional();
    plan.planNutricional.dias = [];
    for (const dia of this.dias) {
      let diaACrear = new Dia();
      diaACrear.descripcion = dia;
      diaACrear.menues = [];
      plan.planNutricional.dias.push(diaACrear);
      for (const menu of this.menus) {
        let menuACrear = new Menu();
        menuACrear.nombre = menu;
        menuACrear.alimentos = [];
        diaACrear.menues.push(menuACrear);
      }
    }
    return plan;
  }

  quitarAlimento(alimento: Alimento, menu: Menu) {
    const index = menu.alimentos.indexOf(alimento);
    menu.alimentos.splice(index);
  }

  agregarAlimento(menu: Menu): void {
    const dialogRef = this.dialog.open(MnAgregarAlimentoComponent, {
      width: '350px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
      const data = result as Alimento;
      console.log(data);
      console.log(result);
      console.log(menu);
      menu.alimentos.push(data);
      console.log(this.plan);
    });
  }

  enviarPlan() {
    let data = this.armarEnvio();
    this.http.realizarPost(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/altaPlanNuevoUsuario', data)
      .subscribe(response => {
        this.presentAlertConfirm('Plan creado exitosamente!');
        this.messageService.sendMessage('reload');
        this.router.navigateByUrl('mn/tabs');
      });
  }

  armarEnvio() {
    console.log(this.plan);
    let count = 0;
    let menuesNuevos: MenuNuevo[];
    menuesNuevos = [];
    // clienteId, descripcion (nombre del plan),
    // menuesNuevos { "menuId": 1, "alimentosMenu": [1,2],"cantidadesAlimentos": [77,88] }
    this.plan.planNutricional.dias.forEach(dia => {
      dia.menues.forEach(menuDia => {
        let menu = new MenuNuevo()
        menu.alimentosMenu = [];
        menu.cantidadesAlimentos = [];
        menu.menuId = count;
        menuDia.alimentos.forEach(alimento => {
          menu.alimentosMenu.push(alimento.alimento.id);
          menu.cantidadesAlimentos.push(alimento.cantidad);
        });
        menuesNuevos.push(menu);
        count = count + 1;
      });
    });

    this.user = this.utils.loadFromLocalStorage('user');
    return { menuesNuevos, clienteId: this.user.id, descripcion: this.dietaNombre};
  }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Okay');

          }
        }
      ]
    });
    await alert.present();
  }

  async crearNuevoPlan() {
    const alert = await this.alertController.create({
      header: 'Ingrese un nombre para la dieta',
      inputs: [
        {
          name: 'dietaNombre',
          type: 'text',
          value: '',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.dietaNombre = data.dietaNombre;
            this.enviarPlan();
          }
        }
      ]
    });

    await alert.present();
  }
}
