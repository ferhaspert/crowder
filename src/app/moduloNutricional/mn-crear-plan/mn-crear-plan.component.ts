import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MnDiarioComponent } from '../mn-diario/mn-diario.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mn-crear-plan',
  templateUrl: './mn-crear-plan.component.html',
  styleUrls: ['./mn-crear-plan.component.scss'],
})
export class MnCrearPlanComponent implements OnInit {

  showError: boolean;

  public planes = [
    {tipo: "Autogenerado", route: 'nuevoPlan/autoGenerado', value: 0},
    {tipo: "Manual", route: 'nuevoPlan/manual', value: 1}
  ];

  tipoPlanSelected = this.planes[0];

  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<MnDiarioComponent>,
    @Inject(MAT_DIALOG_DATA) public modulo: string) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  goToNewPlan(value: any) {
    if (!value) {
      this.showError = true;
      return;
    }
    this.router.navigateByUrl(this.modulo + "/" + value);
    this.dialogRef.close(this.tipoPlanSelected);
  }

  ngOnInit() {
  }

}
