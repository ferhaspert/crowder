import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MnCrearPlanGeneradoComponent } from './mn-crear-plan-generado.component';

describe('MnCrearPlanGeneradoComponent', () => {
  let component: MnCrearPlanGeneradoComponent;
  let fixture: ComponentFixture<MnCrearPlanGeneradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MnCrearPlanGeneradoComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MnCrearPlanGeneradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
