import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import { PlanActivo, Dia, Menu, Alimento } from '../../../interfaces/moduloNutricional/interfaces';
import { DetallePlanNutricionalComponent } from 'app/modals/detalle-plan-nutricional/detalle-plan-nutricional.component';
import { RoutesService } from 'app/services/routes.service';
import { Location } from '@angular/common';
import { ModalService } from 'app/services/modal.service';
import { Router, RouterModule } from '@angular/router';
import { UtilsService } from 'app/services/utils.service';
import { MatDialog } from '@angular/material';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'app-mn-crear-plan-generado',
  templateUrl: './mn-crear-plan-generado.component.html',
  styleUrls: ['./mn-crear-plan-generado.component.scss'],
})
export class MnCrearPlanGeneradoComponent implements OnInit {

  planes: PlanActivo[];
  public user;

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    public routesService: RoutesService,
    private location: Location,
    private modalService: ModalService,
    public router: Router,
    private utils: UtilsService,
    public messageService: MessageService,
  ) { }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.http.realizarGet(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/listarPlanesPredeterminados?clienteId=' + this.user.id)
      .subscribe(result => {
          console.log(result);
        this.planes = result;
        }
      );
  }

  confirmarPlanPredeterminado(plan) {
    let data = {
      "idUsuario": this.user.id,
      "idPlan": plan.id
    }
    this.http.realizarPost(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/altaPlanPredeterminado', data)
      .subscribe(result => {
        if (result.result === 0) {
          this.modalService.openModalSuccess(result.message, () => undefined );
          this.messageService.sendMessage('reload');
          this.router.navigateByUrl('mn/tabs');
        } else {
          this.modalService.openModalError(result.message);
        }
        console.log(result);
      })
  }

  verDetalle(plan): void {
    const dialogRef = this.dialog.open(DetallePlanNutricionalComponent, {
      data: plan,
      panelClass: 'modal-detalle-plan'
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      });
  }

  volver(){
    this.location.back();
  }
}
