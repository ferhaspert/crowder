import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { UtilsService } from 'app/services/utils.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  constructor(public utils: UtilsService, private router: Router) { }

  public getToken(): string {
    return 'holis';
    //return JSON.parse(localStorage.getItem('token'))
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    // return tokenNotExpired(null, token);
    return true;
  }

  public logout() {
    this.utils.saveLocalStorage('user', {});
    this.router.navigate(["/login"]);
  }

}
