import { Injectable, Injector } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { catchError , tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

	constructor(private injector: Injector, private authService: AuthService, private router: Router) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(!request.headers.get("skip")) {  
      if (!!this.authService.getToken()) {
        request = request.clone({
          setHeaders: {
            Authorization: this.authService.getToken()
          }
        });
      }
		}
		return next.handle(request).pipe(
      tap(null, error => {
        if (error.status === 403) {
          this.authService.logout();
        }
      })
    );
	}
}