import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox'; // hay mas, estos son solo de button y check
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatMenuModule } from '@angular/material/menu';
import { Subscription } from 'rxjs';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './login/registro/registro.component';
import { RestablecerPassComponent } from './login/restablecer-pass/restablecer-pass.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { MenuComponent } from './menu/menu/menu.component';
import { CiNutricionalesComponent } from './condicionesIniciales/ci-nutricionales/ci-nutricionales.component';
import { CiDeportivasComponent } from './condicionesIniciales/ci-deportivas/ci-deportivas.component';
import { MnTabsComponent } from './moduloNutricional/mn-tabs/mn-tabs.component';
import { MnDiarioComponent } from './moduloNutricional/mn-diario/mn-diario.component';
import { MnComidaCardComponent } from './moduloNutricional/mn-comida-card/mn-comida-card.component';
import { MnDiaCardComponent } from './moduloNutricional/mn-dia-card/mn-dia-card.component';
import { MnMiPlanComponent } from './moduloNutricional/mn-mi-plan/mn-mi-plan.component';
import { MnPlanesHistorialComponent } from './moduloNutricional/mn-planes-historial/mn-planes-historial.component';
import { MnHistorialCardComponent } from './moduloNutricional/mn-historial-card/mn-historial-card.component';
import { MnAgregarAlimentoComponent } from './moduloNutricional/mn-agregar-alimento/mn-agregar-alimento.component';

import { HttpService } from './services/http.service';
import { MnCrearPlanComponent } from './moduloNutricional/mn-crear-plan/mn-crear-plan.component';
import { RoutesService } from './services/routes.service';
import { MnCrearPlanVentanaComponent } from './moduloNutricional/mn-crear-plan/mn-crear-plan-ventana/mn-crear-plan-ventana.component';
import { MnCrearPlanGeneradoComponent } from './moduloNutricional/mn-crear-plan/mn-crear-plan-generado/mn-crear-plan-generado.component';
import { ModalService } from './services/modal.service';
import { ModalErrorComponent } from './modals/modal-error/modal-error.component';
import { ModalSuccessComponent } from './modals/modal-success/modal-success.component';
import { ModalInfoComponent } from './modals/modal-info/modal-info.component';
import { DetallePlanNutricionalComponent } from './modals/detalle-plan-nutricional/detalle-plan-nutricional.component';
import { MdTabsComponent } from './moduloDeportivo/md-tabs/md-tabs.component';
import { MdPlanesComponent } from './moduloDeportivo/md-planes/md-planes.component';
import { MdEntrenamientoCardComponent } from './moduloDeportivo/md-entrenamiento-card/md-entrenamiento-card.component';
import { MdPlanesAutogeneradosComponent } from './moduloDeportivo/md-crear-plan/md-planes-autogenerados/md-planes-autogenerados.component';
import { DetallePlanDeportivoComponent } from './modals/detalle-plan-deportivo/detalle-plan-deportivo.component';
import { MdPlanesManualesComponent } from './moduloDeportivo/md-crear-plan/md-planes-manuales/md-planes-manuales.component';
import { MdAgregarEjercicioComponent } from './moduloDeportivo/md-agregar-ejercicio/md-agregar-ejercicio.component';
import { MdHistorialComponent } from './moduloDeportivo/md-historial/md-historial.component';
import { ProfesoresTabComponent } from './moduloDeportivo/profesores-tab/profesores-tab.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { SugerenciasComponent } from './sugerencias/sugerencias.component';
// import { AlumnosTabComponent } from './moduloDeportivo/alumnos-tab/alumnos-tab.component';
import { AuthService } from './auth/auth.service';
import { TokenInterceptor } from './auth/token-interceptor.service';
import { LoaderService } from './services/loader.service';
import { MdDiarioComponent } from './moduloDeportivo/md-diario/md-diario.component';
import { MdDetalleEjercicioComponent } from './moduloDeportivo/md-detalle-ejercicio/md-detalle-ejercicio.component';
import { AlumnosTabComponent } from './moduloDeportivo/alumnos-tab/alumnos-tab.component';
import { MdAgregarProfesorComponent } from './moduloDeportivo/md-agregar-profesor/md-agregar-profesor.component';
import { MdDetalleAlumnoComponent } from './moduloDeportivo/md-detalle-alumno/md-detalle-alumno.component';
import { MdDetalleProfesorComponent } from './moduloDeportivo/md-detalle-profesor/md-detalle-profesor.component';
import { MdAsignarPlanComponent } from './moduloDeportivo/md-asignar-plan/md-asignar-plan.component';
import { TextoSimpleComponent } from './modals/texto-simple/texto-simple.component';
import { ModalConfirmarComponent } from './modals/modal-confirmar/modal-confirmar.component';
import { EstatisticasComponent } from './moduloEstadistico/estatisticas/estatisticas.component';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';

@NgModule({
  declarations: [AppComponent, // aca van componentes
    LoginComponent,
    RegistroComponent,
    RestablecerPassComponent,
    DashboardComponent,
    CiDeportivasComponent,
    CiNutricionalesComponent,
    MenuComponent,
    MnTabsComponent,
    MnDiarioComponent,
    MnComidaCardComponent,
    MnDiaCardComponent,
    MnMiPlanComponent,
    MnPlanesHistorialComponent,
    MnHistorialCardComponent,
    MnAgregarAlimentoComponent,
    MnCrearPlanVentanaComponent,
    MnCrearPlanGeneradoComponent,
    MnCrearPlanComponent,
    ModalErrorComponent,
    ModalInfoComponent,
    ModalSuccessComponent,
    DetallePlanNutricionalComponent,
    MdTabsComponent,
    MdPlanesComponent,
    MdEntrenamientoCardComponent,
    MdPlanesAutogeneradosComponent,
    DetallePlanDeportivoComponent,
    MdPlanesManualesComponent,
    MdAgregarEjercicioComponent,
    MdHistorialComponent,
    ProfesoresTabComponent,
    MdDiarioComponent,
    MdDetalleEjercicioComponent,
    AlumnosTabComponent,
    DatosPersonalesComponent,
    SugerenciasComponent,
    MdAgregarProfesorComponent,
    MdDetalleAlumnoComponent,
    MdDetalleProfesorComponent,
    MdAsignarPlanComponent,
    TextoSimpleComponent,
    ModalConfirmarComponent,
    EstatisticasComponent
  ],

  entryComponents: [ MnAgregarAlimentoComponent,
                      MnCrearPlanComponent,
                      ModalErrorComponent,
                      ModalInfoComponent,
                      ModalSuccessComponent,
                      DetallePlanNutricionalComponent,
                      DetallePlanDeportivoComponent,
                      MdAgregarEjercicioComponent,
                      MdDetalleEjercicioComponent,
                      MdDetalleAlumnoComponent,
                      MdDetalleProfesorComponent,
                      MdAgregarProfesorComponent,
                      MdAsignarPlanComponent,
                      TextoSimpleComponent,
                      ModalConfirmarComponent
  ],

  imports: [BrowserModule, // agregar nueva func de angular (Modulos)
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatGridListModule,
    MatTableModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatMenuModule,
    ChartsModule,
    BrowserAnimationsModule,
    MatVideoModule
  ],

  providers: [ // aca importo los services
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HttpService,
    RoutesService,
    ModalService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
