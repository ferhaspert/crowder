import { Injectable } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {

  constructor(public router: Router) { }

 
  public moduloLogin = "http://localhost:7076";
  public moduloNutricional = "http://localhost:7077";
  public moduloDeportivo = "http://localhost:7078/nutrischool/gestionDeportiva/";
  public moduloEstadistico= "http://localhost:7079/nutrischool/gestionEstadistica/";
 
  // public moduloLogin = "http://167.172.128.232:7076";
  // public moduloNutricional = "http://167.172.128.232:7077";
  // public moduloDeportivo = "http://167.172.128.232:7078/nutrischool/gestionDeportiva/";
  // public moduloEstadistico= "http://167.172.128.232:7079/nutrischool/gestionEstadistica/";

  navigateByUrl(url) {
    
  }
}
