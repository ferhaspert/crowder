import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError , tap } from 'rxjs/operators';

import { LoginResponse, CreateUserResponse } from 'app/interfaces/login/login-response';
import { LoginRequest } from 'app/interfaces/login/login-request';
import { PlanActivo, Alimento } from 'app/interfaces/moduloNutricional/interfaces';
import { RoutesService } from './routes.service';
import { AuthService } from 'app/auth/auth.service';
import { LoaderService } from './loader.service';
import { ModalService } from './modal.service';

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(private http: HttpClient, 
    private routesService: RoutesService,
    private loader: LoaderService,
    private modalService: ModalService
     ) {}

  public realizarGet(url: string, opciones?: any, omitirLoader?: boolean): any {
    this.loader.present();
    return this.http.get(url, opciones).pipe(
      tap(_ => this.loader.dismiss()),
      catchError((error: HttpErrorResponse) => {
        // console.log(error);
        this.loader.dismiss();
        return this.manejarError(error);
      })
    );
  }

  public realizarPost(url: string, datos: any, omitirLoader?: boolean, opciones?: any): any {
    this.loader.present();
    return this.http.post(url, datos, opciones).pipe(
      tap(_ => this.loader.dismiss()),
      catchError((error: HttpErrorResponse) => {
        console.log(error, "error");
        this.loader.dismiss();
        return this.manejarError(error);
      })
    );
  }

  public realizarPostSinToken(url: string, datos: any, omitirLoader?: boolean, opciones?: any): any {
    let headers = new HttpHeaders({'skip': "true" });
    opciones = { headers: headers };
    return this.http.post(url, datos, opciones).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.manejarError(error);
      })
    );
  }

  private manejarError(error: HttpErrorResponse) {
    var mensaje = error.message || "Ha ocurrido un error inesperado.";
    if(error.name == "HttpErrorResponse") {
      var mensaje = "No pudimos comunicarnos con el servidor, por favor intente más tarde."
    }
    if(error.status != 403) {
      this.modalService.openModalError(mensaje);
    }
    return throwError(error);
  }

   tryLogin(data: LoginRequest): Observable<LoginResponse> {
     return this.realizarPostSinToken(this.routesService.moduloLogin + '/nutrischool/gestionUsuario/login', data);
   }

   createUser(data: any): Observable<any> {
     return this.realizarPostSinToken(this.routesService.moduloLogin + '/nutrischool/gestionUsuario/registrarUsuario', data);
   }

   resetPassword(data: any): Observable<any> {
     return this.realizarPost(this.routesService.moduloLogin + '/nutrischool/gestionUsuario/restablecerPassword', data);
   }

    listarPlanActivoUsuario(idUsuario: string, data?: any): Observable<PlanActivo> {
     return this.realizarGet(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/listarPlanActivoUsuario?idUsuario=' + idUsuario, data);
   }

   getFoods(searchQuery: string, data?: any): Observable<Alimento[]> {
     return this.realizarGet(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/listarAlimentos?nombreAlimento=' + searchQuery, data);
   }

   agregarNuevoPlan(data: any) {
     return this.realizarPost(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/altaPlanNuevoUsuario', data);
   }

   obtenerAlumnosActivos(data: number) {
     return this.realizarGet(this.routesService.moduloDeportivo + 'getAlumnosActivos?profId=' + data);
   }

   getSolicitudesPendientes(data: number) {
     return this.realizarGet(this.routesService.moduloDeportivo + 'getSolicitudesPendientes?profId=' + data);
   }

   aceptarAlumno(clienteId: number, profUserId: number) {
     return this.realizarPost(this.routesService.moduloDeportivo + 'aceptarAlumno?clienteId=' + clienteId + '&profUserId=' + profUserId, null);
   }

   rechazarAlumno(clienteId: number, profUserId: number) {
     return this.realizarPost(this.routesService.moduloDeportivo + 'bajaAlumno?clienteId=' + clienteId + '&profUserId=' + profUserId, null);
   }

   listarAlimentosSinLoader(searchString: string): any {
     return this.http.get(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/listarAlimentos?nombreAlimento=' + searchString);
   }

   listarEjerciciosSinLoader(searchString: string): any {
     return this.http.get(this.routesService.moduloDeportivo + "listarEjercicios?nombreEjercicio=" + searchString);
   }

   listarProfesoresSinLoader(searchString: string): any {
      return this.http.get(this.routesService.moduloDeportivo + "listarProfesores?profId=" + searchString)
   }

   post(url, data) {
     return this.http.post(url, data);
   }
}
