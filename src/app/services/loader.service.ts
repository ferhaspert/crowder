import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  isLoading = false;
  text: string;
  loader: any;

  constructor(public loadingController: LoadingController) { }

  async present() {
    if(!this.isLoading) {
      this.isLoading = true;
      return await this.loadingController.create({
        message: 'Cargando...'
      }).then(a => {
        a.present().then(() => {
          console.log('presented');
          if (!this.isLoading) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
    }
  }

  async dismiss() {
    this.isLoading = false;
    // await this.loadingController.dismiss();
    return await this.loadingController.dismiss(null, undefined).then(() => console.log('dismissed'));
  }

  // showModal() {
  //   this.loader = this.loadingController.create({
  //     message: 'Please wait...'
  //   });
  //   this.loader.present();
  // }

  // hideModal() {
  //   //alert("hide modal");
  //   this.loader.dismiss().catch(() => {});
  // }
}