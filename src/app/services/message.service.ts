import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  myMessage = new Subject<any>();
  constructor() { }

  getMessage(): Observable<any> {
    return this.myMessage.asObservable();
  }

  sendMessage(message: any) {
    this.myMessage.next(message);
  }
}

@Injectable({
  providedIn: 'root'
})
export class DeportivoMessageService {
  myMessage = new Subject<any>();
  constructor() { }

  getMessage(): Observable<any> {
    return this.myMessage.asObservable();
  }

  sendMessage(message: any) {
    this.myMessage.next(message);
  }
}
