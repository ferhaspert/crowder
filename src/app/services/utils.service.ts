import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UtilsService {

  public planes: any[] = []

  public days = {
    1: 'Lunes',
    2: 'Martes',
    3: 'Miércoles',
    4: 'Jueves',
    5: 'Viernes',
    6: 'Sábado',
    7: 'Domingo',
  };
  constructor() { }

  saveLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  loadFromLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  setPlanesActivos(planes) {
    this.planes = planes;
  }

  getPlanesActivos() {
    return this.planes;
  }
}
