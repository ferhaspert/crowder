import { Injectable, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalErrorComponent } from 'app/modals/modal-error/modal-error.component';
import { ModalSuccessComponent } from 'app/modals/modal-success/modal-success.component';
import { Router, RouterModule } from '@angular/router';
import { TextoSimpleComponent } from 'app/modals/texto-simple/texto-simple.component';
import { ModalConfirmarComponent } from 'app/modals/modal-confirmar/modal-confirmar.component';
import { ModalInfoComponent } from 'app/modals/modal-info/modal-info.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(public dialog: MatDialog) { }

  private dialogRefError: any;
  private dialogOpen = false;

  openModalError(message): void {
    if(!this.dialogOpen) {
      this.dialogOpen = true;
      this.dialogRefError = this.dialog.open(ModalErrorComponent, {
        panelClass: 'modal-error-container',
        data: message
      });
  
      this.dialogRefError.afterClosed().subscribe(data => {
        this.dialogOpen = false;
      });
    }
  }

  openModalInfo(message): void {
    if(!this.dialogOpen) {
      this.dialogOpen = true;
      this.dialogRefError = this.dialog.open(ModalInfoComponent, {
        panelClass: 'modal-error-container',
        data: message
      });
  
      this.dialogRefError.afterClosed().subscribe(data => {
        this.dialogOpen = false;
      });
    }
  }

  openModalSuccess(message, funcionAlCerrar): void {
    const dialogRef = this.dialog.open(ModalSuccessComponent, {
      panelClass: 'modal-error-container',
      data: message
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      funcionAlCerrar();
    });
  }

  openModalTextoSimple(titulo, texto): void {
    const dialogRef = this.dialog.open(TextoSimpleComponent, {
      panelClass: 'modal-texto-simple',
      data: {
        "titulo": titulo, "texto": texto 
      }
    });
  }
}
