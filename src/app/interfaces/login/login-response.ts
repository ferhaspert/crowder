export interface LoginResponse {
    message: string;
    result: number;
    tipoUsuario: string;
    token: string;
    usuarioAplicacion: {
      apellido: string,
      estado: number,
      fechaDeNacimiento: {
        date: number,
        day: number,
        hours: number,
        minutes: number,
        month: number,
        nanos: number,
        seconds: number,
        time: number,
        timezoneOffset: number,
        year: number
      },
      id: number,
      mail: string,
      nombre: string,
      password: string,
      rol: number,
      userLogin: string
    };
}

export interface CreateUserResponse {
    message: string;
    result: number;
}
