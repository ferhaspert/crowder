export class PlanActivo {
    // cliente: Cliente;
    estado: number;
    vencido: boolean;
    planNutricional: PlanNutricional
}

export class PlanNutricional {
    descripcion: string;
    fecha: Date;
    dias: Dia[];
    id: number;
    objetivoAsociado: number;
    predeterminado: boolean;
}

export class Cliente {
  apellido: string;
  estado: number;
  fechaDeNacimiento: Date;
  id: number;
  mail: string;
  objetivoDeportivo: string;
  objetivoNutricional: string;
  password: string;
  peso: number;
  profesores: [];
  rol: number;
  userLogin: string;
  vinculado: boolean;
}

export class Dia {
  descripcion: string;
  menues: Menu[];
  id?: number;
}

export class Menu {
  alimentos: Alimento[];
  menuId?: number;
  nombre: string;
  id?: number;
}

export class Alimento {
  alimento: AlimentoReal;
  cantidad: number;
}

export class AlimentoReal {
  calorias: number;
  fibra: number;
  id: number;
  nombre: string;
  tipoPorcion: string;
}

export class MenuNuevo {
  menuId: number;
  alimentosMenu: number[];
  cantidadesAlimentos: number[];
}