export class PlanDeportivo {
    cantidadDias: number;
    clienteId: number;
    descripcion: string;
    entrenamientos: Entrenamiento[];
}

export class Ejercicio {
    aerobico?: boolean;
    duracion?: number;
    id?: number;
    peso?: number;
    repeticiones?: number;
    series?: number;
    valid?: boolean;
    tipo?: string;
}

export class Entrenamiento {
    ejercicios: Ejercicio[];
    nombreEntrenamiento?: string;
    nombreDia?: string;
}
