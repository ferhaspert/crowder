import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UtilsService } from 'app/services/utils.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  private user;
  constructor(
    private menu: MenuController,
    private router: Router,
    private utils: UtilsService,
  ) { }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  cerrarSesion() {
    this.utils.saveLocalStorage('user', {});
    this.router.navigateByUrl('login');
  }

  esCliente(){
    return (this.user.rol == 2)
  }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    console.log(this.user);
  }
}
