import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-detalle-plan-deportivo',
  templateUrl: './detalle-plan-deportivo.component.html',
  styleUrls: ['./detalle-plan-deportivo.component.scss'],
})
export class DetallePlanDeportivoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetallePlanDeportivoComponent>,
    @Inject(MAT_DIALOG_DATA) public plan: any,
    public httpService: HttpService,
    public routeService: RoutesService,
    public modaleService: ModalService, 
    public router: Router) { }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  navigateHome() {
    this.router.navigateByUrl('md/tabs');
  }

  close() {
    this.dialogRef.close(this.plan);
  }

  activar() {
    this.dialogRef.close(this.plan);
    var data = {
      "idPlan": this.plan.id,
      "idUsuario": JSON.parse(localStorage.getItem('user')).id
  }
    this.httpService.realizarPost(this.routeService.moduloDeportivo + "altaPlanPredeterminado", data)
    .subscribe(response => {
      if(response.result == 0) {
        this.modaleService.openModalSuccess(response.message, this.navigateHome.bind(this));
      }
      else {
        this.modaleService.openModalError(response.message);
      }
      console.log(response);
    })
  }
}
