import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';

@Component({
  selector: 'app-detalle-plan-nutricional',
  templateUrl: './detalle-plan-nutricional.component.html',
  styleUrls: ['./detalle-plan-nutricional.component.scss'],
})
export class DetallePlanNutricionalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetallePlanNutricionalComponent>,
    @Inject(MAT_DIALOG_DATA) public plan: any,
    public httpService: HttpService,
    public routeService: RoutesService) { }

  ngOnInit() {
    this.plan.dias.sort((a, b) => a.id - b.id);
    for (let dia of this.plan.dias) {
      dia.menues.sort((a, b) => a.id - b.id);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close(this.plan);
  }
}
