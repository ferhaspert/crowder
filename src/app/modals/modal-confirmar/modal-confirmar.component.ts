import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-confirmar',
  templateUrl: './modal-confirmar.component.html',
  styleUrls: ['./modal-confirmar.component.scss'],
})
export class ModalConfirmarComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public texto: any,
    public dialogRef: MatDialogRef<ModalConfirmarComponent>
  ) { }

  ngOnInit() {}

  close() {
    this.dialogRef.close(false);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  confirmar() {
    this.dialogRef.close(true);
  }

  cancelar() {
    this.dialogRef.close(false);
  }
}
