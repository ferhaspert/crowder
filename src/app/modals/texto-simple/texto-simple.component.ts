import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-texto-simple',
  templateUrl: './texto-simple.component.html',
  styleUrls: ['./texto-simple.component.scss'],
})
export class TextoSimpleComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public info: any,
    public dialogRef: MatDialogRef<TextoSimpleComponent>) { }

  ngOnInit() {}

  close() {
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
