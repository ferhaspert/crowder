import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  private user;
  public mySubscription: any;
  constructor(
    private utils: UtilsService, public router: Router
  ) {

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        if(event.url == "/dashboard") { this.ngOnInit() }
      }
    });

   }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    console.log(this.user);
  }

  esMasculino(){
    return (this.user.sexo == 'M')
  }

  esCliente(){
    return (this.user.rol == 2)
  }

}
