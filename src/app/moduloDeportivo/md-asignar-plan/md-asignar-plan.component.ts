import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'app/services/utils.service';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-md-asignar-plan',
  templateUrl: './md-asignar-plan.component.html',
  styleUrls: ['./md-asignar-plan.component.scss'],
})
export class MdAsignarPlanComponent implements OnInit {
  @Input() public alumno;
  public planesActivos;
  public example;
  public dataLoaded;

  constructor(
    public httpService: HttpService,
    public routeService: RoutesService,
    public alertController: AlertController,
    public utils: UtilsService,
    public modalCtrl: ModalController,
    public router: Router,
    public location: Location,
    public modalService: ModalService,
  ) { }

  ngOnInit() {
    this.httpService
      .realizarGet(this.routeService.moduloDeportivo + 'getPlanesActivosProfesor?profId=' +
        this.utils.loadFromLocalStorage('user').id)
      .subscribe(resultado => {
        this.dataLoaded = true;
        this.planesActivos = resultado;
        console.log(this.planesActivos);
        console.log(this.modalCtrl);
      });
  }

  async presentAlertConfirm(fallbackTrue, fallbackFalse = () => undefined) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: '¿Asignar plan a usuario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            if (fallbackFalse) {
              fallbackFalse();
            }
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            if (fallbackTrue) {
              fallbackTrue();
            }

          }
        }
      ]
    });

    await alert.present();
  }

  asignarPlanAlumno(plan) {
    const fallbackTrue = () => {
      this.httpService.realizarPost(this.routeService.moduloDeportivo + 'asignarPlanAUsuario',
        {idPlan: plan.planDeportivo.id, idUsuario: this.alumno.id})
        .subscribe(resultado => {
          if(resultado.result == 0) {
            this.modalService.openModalSuccess(resultado.message, {});
          }else {
            this.modalService.openModalError(resultado.message);
          }
          this.modalCtrl.dismiss();
          //this.mostrarOkMensaje('Se asignó el plan correctamente!', () => this.modalCtrl.dismiss());
        }
        );
    };
    this.presentAlertConfirm(fallbackTrue);
  }

  async mostrarOkMensaje(message: string, fallbackTrue = () => undefined) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          handler: () => {
            fallbackTrue();
          }
        }
      ]
    });

    await alert.present();
  }

  volver() {
    this.modalCtrl.dismiss();
  }

}
