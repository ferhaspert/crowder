import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { MatDialog } from '@angular/material/dialog';
import { MnCrearPlanComponent } from 'app/moduloNutricional/mn-crear-plan/mn-crear-plan.component';
import { UtilsService } from 'app/services/utils.service';
import { DeportivoMessageService } from 'app/services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-md-planes',
  templateUrl: './md-planes.component.html',
  styleUrls: ['./md-planes.component.scss'],
})
export class MdPlanesComponent implements OnInit {

  public planesActivos: any[] = [];
  public user;
  public subscription;

  constructor(
    public http: HttpService,
    public routeService: RoutesService,
    public dialog: MatDialog,
    private utils: UtilsService,
    private messageService: DeportivoMessageService,
    private router: Router,
  ) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        if (message === 'reload') {
          this.ngOnInit();
        }
      }
    });
  }

  ngOnInit() {
    let service = "";
    this.user = this.utils.loadFromLocalStorage('user');

    if(this.user != null && this.user.rol == 2){
      service = "getPlanesActivosUsuario?clienteId=";
    }else if(this.user != null && this.user.rol == 3){
      service = "getPlanesActivosProfesor?profId=";
    }

    this.http
      .realizarGet(this.routeService.moduloDeportivo + service + JSON.parse(localStorage.getItem('user')).id)
      .subscribe(resultado => {
        this.planesActivos = resultado;
        this.utils.setPlanesActivos(this.getPlanesIds(resultado))
        this.planesActivos.forEach(plan => {
            plan.visible = false;
        });
        if(this.planesActivos.length > 0) {
          this.planesActivos[0].visible = true;
        }
        console.log(resultado)
      });
  }

  getPlanesIds(planes) {
    var ids = [];
    planes.forEach(plan => {
      ids.push(plan.planDeportivo.id)
    });
    return ids;
  }

  esCliente() {
    return (this.user.rol == 2);
  }

  crearNuevoPlan() {
    if(this.esCliente()){
      const dialogRef = this.dialog.open(MnCrearPlanComponent, {
          width: '350px',
          data: 'md'
        });
    }else{
      this.router.navigateByUrl("md/nuevoPlan/manual");
    }
  }

  toggleVisible(plan) {
      plan.visible = !plan.visible;
  }
}
