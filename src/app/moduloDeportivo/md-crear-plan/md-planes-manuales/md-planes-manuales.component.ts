import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Router, RouterModule } from '@angular/router';
import { MdAgregarEjercicioComponent } from 'app/moduloDeportivo/md-agregar-ejercicio/md-agregar-ejercicio.component';
import { Entrenamiento, Ejercicio, PlanDeportivo } from '../../../interfaces/moduloDeportivo/interfaces-deportivo';
import { UtilsService } from '../../../services/utils.service';
import { ModalService } from 'app/services/modal.service';
import { DeportivoMessageService } from 'app/services/message.service';

@Component({
  selector: 'app-md-planes-manuales',
  templateUrl: './md-planes-manuales.component.html',
  styleUrls: ['./md-planes-manuales.component.scss'],
})
export class MdPlanesManualesComponent implements OnInit {

  public planDeportivo: PlanDeportivo = new PlanDeportivo();
  public invalidNameForm = false;

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    public routeService: RoutesService,
    public router: Router,
    public utils: UtilsService,
    private modalService: ModalService,
    private messageService: DeportivoMessageService,
  ) { }

  ngOnInit() {
    this.planDeportivo.entrenamientos = [];
  }

  agregarEjercicio(entrenamiento): void {
    const dialogRef = this.dialog.open(MdAgregarEjercicioComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(ejercicio => {
      if (ejercicio) {
        entrenamiento.ejercicios.push(ejercicio)
      }
    });
  }

  nuevoEntrenamiento() {
    var entrenamiento = new Entrenamiento()
    entrenamiento.nombreEntrenamiento = "";
    entrenamiento.ejercicios = []
    this.planDeportivo.entrenamientos.push(entrenamiento)
  }

  cancelar() {
    this.router.navigateByUrl('md/tabs');
  }

  armarPlan() {

    var dias: any[] = [];
    this.planDeportivo.entrenamientos.forEach(entrenamiento => {
      dias.push({"entrenamientos": entrenamiento.ejercicios, "nombreDia": entrenamiento.nombreEntrenamiento})
    });

    return {
      "cantidadDias": this.planDeportivo.entrenamientos.length,
      "clienteId": JSON.parse(localStorage.getItem('user')).id,
      "descripcion": this.planDeportivo.descripcion,
      "dias": dias
    };
  }

  crearPlan() {
    this.invalidNameForm = false;
    console.log(this.planDeportivo.descripcion);
    if (!this.planDeportivo.descripcion) {
      this.invalidNameForm = true;
      return;
    }
    var data = this.armarPlan();
    this.http.realizarPost(this.routeService.moduloDeportivo + "altaPlanNuevo", data)
      .subscribe(response => {
        if(response.result == 0) {
          this.modalService.openModalSuccess(response.message, this.cancelar.bind(this))
          this.messageService.sendMessage('reload');
        }
        else {
          this.modalService.openModalError(response.message)
        }
      })
  }
}
