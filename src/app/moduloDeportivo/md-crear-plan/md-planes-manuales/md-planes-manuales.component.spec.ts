import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdPlanesManualesComponent } from './md-planes-manuales.component';

describe('MdPlanesManualesComponent', () => {
  let component: MdPlanesManualesComponent;
  let fixture: ComponentFixture<MdPlanesManualesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdPlanesManualesComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdPlanesManualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
