import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material/dialog';
import { DetallePlanDeportivoComponent } from 'app/modals/detalle-plan-deportivo/detalle-plan-deportivo.component';
import { UtilsService } from 'app/services/utils.service';
import { ModalService } from 'app/services/modal.service';
import { Router } from '@angular/router';
import { DeportivoMessageService } from 'app/services/message.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-md-planes-autogenerados',
  templateUrl: './md-planes-autogenerados.component.html',
  styleUrls: ['./md-planes-autogenerados.component.scss'],
})
export class MdPlanesAutogeneradosComponent implements OnInit {

  public planes: any[] = [];
  public user: any;
  public condiciones = [
    {id: 26, descripcion: 'Running'},
    {id: 36, descripcion: 'Natación'},
    {id: 46, descripcion: 'Musculación Principiante'},
    {id: 45, descripcion: 'Musculación Avanzada'},
    {id: 44, descripcion: 'Musculación Reducción de Peso'},
  ];
  public condicionSeleccionada;

  constructor(
    public dialog: MatDialog,
    public http: HttpService,
    public routesService: RoutesService,
    private utils: UtilsService,
    private modalService: ModalService,
    public router: Router,
    private messageService: DeportivoMessageService,
    private location: Location,
  ) { }

  listarPlanesPredeterminados(condiciones) {
    this.http.realizarGet(this.routesService.moduloDeportivo + 'listarPlanesPredeterminados?clienteId=' + this.user.id + '&condicionesIniciales=' + condiciones)
      .subscribe(result => {
        console.log(result);
        this.planes = this.filtrarPlanes(result);
      });
  }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.condicionSeleccionada = this.condiciones[0];
    this.listarPlanesPredeterminados(this.condicionSeleccionada.id);
  }

  filtrarPlanes(planes) {
    var planesFiltrados = [];
    var planesActivos = this.utils.getPlanesActivos();
    planes.forEach(plan => {
      var estaActivo = false;
      planesActivos.forEach(idPlanActivo => {
        if (plan.id == idPlanActivo) {
          estaActivo = true;
        }
      });
      if (!estaActivo) {
        planesFiltrados.push(plan)
      }
    });
    return planesFiltrados
  }

  esCliente() {
    return (this.user.rol == 2);
  }

  verDetalle(plan): void {
    const dialogRef = this.dialog.open(DetallePlanDeportivoComponent, {
      data: plan,
      panelClass: 'modal-detalle-plan'
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      });
  }

  navigateHome() {
    this.router.navigateByUrl('md/tabs');
  }

  cambiarCondicion(condicion) {
    this.condicionSeleccionada = condicion;
    this.listarPlanesPredeterminados([condicion.id]);
  }

  activar(plan) {
    let clienteId = this.user.id;
    let planId = plan.id;
    var data = {
      "idPlan": planId,
      "idUsuario": clienteId
    }
    this.http.realizarPost(this.routesService.moduloDeportivo + '/altaPlanPredeterminado', data)
      .subscribe(response => {
        if (response.result === 0) {
          this.modalService.openModalSuccess(response.message, this.navigateHome.bind(this));
          this.messageService.sendMessage('reload');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
  }

  volver(){
    this.location.back();
  }
}
