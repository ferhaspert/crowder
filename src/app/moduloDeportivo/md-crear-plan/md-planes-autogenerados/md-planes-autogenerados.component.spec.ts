import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdPlanesAutogeneradosComponent } from './md-planes-autogenerados.component';

describe('MdPlanesAutogeneradosComponent', () => {
  let component: MdPlanesAutogeneradosComponent;
  let fixture: ComponentFixture<MdPlanesAutogeneradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdPlanesAutogeneradosComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdPlanesAutogeneradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
