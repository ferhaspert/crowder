import { Component, OnInit, Inject, ErrorHandler } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-md-detalle-ejercicio',
  templateUrl: './md-detalle-ejercicio.component.html',
  styleUrls: ['./md-detalle-ejercicio.component.scss'],
})
export class MdDetalleEjercicioComponent implements OnInit, ErrorHandler {

  constructor(
    @Inject(MAT_DIALOG_DATA) public ejercicio: any,
    public dialogRef: MatDialogRef<MdDetalleEjercicioComponent>
  ) { }

  ngOnInit() {
  }

  handleError(error: Error) {
    // Do whatever you like with the error (send it to the server?)
    // And log it to the console
    console.error('It happens: ', error);
 }

  public videosDisponibles = [1, 18, 19, 23, 26, 45, 54, 55, 141];
  public disponible = this.videosDisponibles.includes(this.ejercicio.ejercicio.id);
  public rutaVideo = "/assets/ejercicios/" + this.ejercicio.ejercicio.id + ".mp4";

  close() {
    this.dialogRef.close();
  }

}
