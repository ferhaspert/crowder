import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'app/services/utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-md-tabs',
  templateUrl: './md-tabs.component.html',
  styleUrls: ['./md-tabs.component.scss'],
})
export class MdTabsComponent implements OnInit {

  public user;

  constructor(
    private utils: UtilsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    if(this.user.rol == 2 && (!this.user.condiciones ||  this.user.condiciones.length === 0)){
      this.router.navigateByUrl('condicionesIniciales/nutricionales');
    }
    console.log(this.user);
  }

}
