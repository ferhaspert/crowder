import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { MdAgregarEjercicioComponent } from '../md-agregar-ejercicio/md-agregar-ejercicio.component';
import { UtilsService } from 'app/services/utils.service';
import { Router, RouterModule } from '@angular/router';
import { MdDetalleEjercicioComponent } from '../md-detalle-ejercicio/md-detalle-ejercicio.component';
import { ModalConfirmarComponent } from 'app/modals/modal-confirmar/modal-confirmar.component';
import { DeportivoMessageService } from 'app/services/message.service'

@Component({
  selector: 'app-md-diario',
  templateUrl: './md-diario.component.html',
  styleUrls: ['./md-diario.component.scss'],
})
export class MdDiarioComponent implements OnInit {

  public subscription;
  constructor(
    public dialog: MatDialog,
    public httpService: HttpService,
    public routeService: RoutesService,
    public router: Router,
    public utils: UtilsService,
    private modalService: ModalService,
    private messageService: DeportivoMessageService,
  ) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        if (message === 'reload') {
          this.ngOnInit();
        }
      }
    });
  }

    public entrenamientos: any = {
        "entrenamientosDelDia": [],
        "entrenamientosRealizadosDelDia": []
    };

    public visible = {
        "realizados": false,
        "noRealizados": true
    }

    ngOnInit() {
        this.httpService.realizarGet(
        this.routeService.moduloDeportivo + "getDiarioDeportivo?clienteId=" + JSON.parse(localStorage.getItem('user')).id)
        .subscribe(resultado => {
          // TODO abrir modal en caso de que no tenga planes activos
          this.entrenamientos = resultado;
          this.setSinModificar(this.entrenamientos.entrenamientosDelDia);
          this.setSinModificar(this.entrenamientos.entrenamientosRealizadosDelDia);
          this.entrenamientos.entrenamientosRealizadosDelDia.forEach(entrenamiento => {
              entrenamiento.ejercicios.forEach(ejercicio => {
                  ejercicio.ejercicio = ejercicio.ejercicioReal;
              });
          });
          console.log(resultado)
        }); 
    }

    setSinModificar(entrenamientos) {
        entrenamientos.forEach(entrenamiento => {
            entrenamiento.modificado = false;
        });
    }

    toggleVisible(tipo) {
        this.visible[tipo] = !this.visible[tipo];
    }

    armarNuevoEjercicio(ejercicio) {
        var nuevoEjercicio: any = {}; 
        nuevoEjercicio.ejercicio = {
            "aerobico": ejercicio.aerobico,
            "descripcion": ejercicio.descripcion,
            "grupoMuscular": ejercicio.grupoMuscular,
            "id": ejercicio.id,
            "visual": ejercicio.visual
        }
        nuevoEjercicio.peso = ejercicio.peso;
        nuevoEjercicio.repeticiones = ejercicio.repeticiones;
        nuevoEjercicio.series = ejercicio.series;
        nuevoEjercicio.duracion = ejercicio.duracion;
        return nuevoEjercicio;
    }

    openTextoSimple() {
        this.modalService.openModalTextoSimple("Lo sentimos...", "No pudimos conectarnos con el servidor. Por favor intente más tarde.");
    }

    agregarEjercicio(entrenamiento): void {
        const dialogRef = this.dialog.open(MdAgregarEjercicioComponent, {
        width: '350px'
        });

        dialogRef.afterClosed().subscribe(ejercicio => {
            // var nuevoEjercicio = armarNuevoEjercicio(ejercicio)
            entrenamiento.ejercicios.push(this.armarNuevoEjercicio(ejercicio))
            entrenamiento.modificado = true;
        });
    }

    verDetalleEjercicio(ejercicio) {
        const dialogRef = this.dialog.open(MdDetalleEjercicioComponent, {
            width: '350px',
            data: ejercicio
        });
    }

    navigateHome() {
        this.router.navigateByUrl('md/tabs');
    }

    armarRegistroDeProgreso(entrenamiento) {
        var ejercicios = [];
        entrenamiento.ejercicios.forEach(entrenamiento => {
            ejercicios.push({
                "aerobico": entrenamiento.ejercicio.aerobico,
                "duracion": entrenamiento.duracion,
                "id": entrenamiento.ejercicio.id,
                "peso": entrenamiento.peso,
                "repeticiones": entrenamiento.repeticiones,
                "series": entrenamiento.series
            })
        });
        return {
            "clienteId": JSON.parse(localStorage.getItem('user')).id,
            "cumplido": !entrenamiento.modificado,
            "diaEntrenamientoId": entrenamiento.id,
            "entrenamientos": ejercicios
        }
    }

    openConfirmarEntrenamiento(entrenamiento): any {
        const dialogRef = this.dialog.open(ModalConfirmarComponent, {
            panelClass: 'modal-confirmar-container',
            data: "¿Desea confirmar el entrenamiento?"
        });

        dialogRef.afterClosed().subscribe(confirmado => {
            if(confirmado) {
                this.confirmarEntrenamiento(entrenamiento)
            }
        });
    }

    confirmarEntrenamiento(entrenamiento) {
        var data = this.armarRegistroDeProgreso(entrenamiento)
        this.httpService.realizarPost(this.routeService.moduloDeportivo + "registrarProgresoDeportivo", data)
        .subscribe(response => {
          if(response.result == 0) {
            this.setRealizado(entrenamiento);
            this.modalService.openModalSuccess(response.message, this.navigateHome.bind(this));
          }
          else {
            this.modalService.openModalError(response.message);
          }
          console.log(response);
        })
    }

    setRealizado(entrenamiento) {
        var index = this.entrenamientos.entrenamientosDelDia.indexOf(entrenamiento)
        if (index > -1) {
            this.entrenamientos.entrenamientosDelDia.splice(index, 1);
            this.entrenamientos.entrenamientosRealizadosDelDia.push(entrenamiento);
        }
    }
}
