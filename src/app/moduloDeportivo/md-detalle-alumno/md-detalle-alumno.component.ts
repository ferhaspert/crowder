import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { MdAsignarPlanComponent } from 'app/moduloDeportivo/md-asignar-plan/md-asignar-plan.component';

@Component({
  selector: 'app-md-detalle-alumno',
  templateUrl: './md-detalle-alumno.component.html',
  styleUrls: ['./md-detalle-alumno.component.scss'],
})
export class MdDetalleAlumnoComponent implements OnInit {
  @Input() public alumno;
  public planesActivos;
  public dialogRef: MatDialogRef<MdDetalleAlumnoComponent>;
  public example;
  public dataLoaded;

  displayedColumns: string[] = ['descripcion', 'series', 'repeticiones', 'duracion'];

  constructor(
    public http: HttpService,
    public routeService: RoutesService,
    public router: Router,
    private modalCtrl: ModalController,
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {example: string};
    this.alumno = state.example;
  }

  ngOnInit() {
    console.log(this.alumno);
    this.http
      .realizarGet(this.routeService.moduloDeportivo + 'getPlanesActivosUsuario?clienteId=' +
      this.alumno.id)
      .subscribe(resultado => {
        this.planesActivos = resultado;
        this.dataLoaded = true;
        console.log(this.planesActivos);
  });
  }

  async agregarPlanAlumno(alumno) {
    const modal = await this.modalCtrl.create({
      component: MdAsignarPlanComponent,
      componentProps: {
        alumno, // Es lo mismo que 'alumno': alumno
    }
    });
    return await modal.present();
  }
}
