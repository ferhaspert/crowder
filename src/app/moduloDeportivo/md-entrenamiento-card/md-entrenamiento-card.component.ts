import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpService } from 'app/services/http.service';

@Component({
  selector: 'app-md-entrenamiento-card',
  templateUrl: './md-entrenamiento-card.component.html',
  styleUrls: ['./md-entrenamiento-card.component.scss'],
})
export class MdEntrenamientoCardComponent implements OnInit {

  @Input() entrenamiento: any;

  constructor(
    public dialog: MatDialog,
    public http: HttpService
  ) { }

  ngOnInit() {}

}
