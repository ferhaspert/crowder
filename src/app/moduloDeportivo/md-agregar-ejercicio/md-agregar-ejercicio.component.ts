import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, switchMap } from 'rxjs/operators';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Entrenamiento, Ejercicio, PlanDeportivo } from '../../interfaces/moduloDeportivo/interfaces-deportivo';

@Component({
    selector: 'app-md-agregar-ejercicio',
    templateUrl: './md-agregar-ejercicio.component.html',
    styleUrls: ['./md-agregar-ejercicio.component.scss'],
})

export class MdAgregarEjercicioComponent implements OnInit {

    public ejercicio: Ejercicio = new Ejercicio();
    public invalidNameForm = false;

    constructor(
        public dialogRef: MatDialogRef<MdAgregarEjercicioComponent>,
        @Inject(MAT_DIALOG_DATA) public entrenamiento: Entrenamiento,
        public httpService: HttpService,
        public routeService: RoutesService
        ) { }

    filteredOptions: Observable<any[]>;

    ngOnInit() {
        this.filteredOptions = this.ejercicioForm.valueChanges
        .pipe(
          debounceTime(200),
          switchMap(value => {
            return this._filter(value);
          }),
        );
    }

    ejercicioForm = new FormControl();
    pesoForm = new FormControl();
    seriesForm = new FormControl();
    repeticionesForm = new FormControl();
    duracionForm = new FormControl();

    onNoClick(): void {
        this.dialogRef.close();
    }

    public options: any[] = []

    public agregar() {
        this.invalidNameForm = false;
        if (!this.ejercicio.aerobico) {
            if (this.repeticionesForm.value == null || this.seriesForm.value == null) {
                this.invalidNameForm = true;
                return;
            }
            if (this.repeticionesForm.value <= 0 || this.seriesForm.value <= 0) {
                this.invalidNameForm = true;
                return;
            }
            this.ejercicio.repeticiones = this.repeticionesForm.value;
            this.ejercicio.series = this.seriesForm.value;
            this.ejercicio.peso = this.pesoForm.value;
            this.ejercicio.duracion = 0;
        } else if (this.ejercicio.aerobico) {
            if (this.duracionForm.value == null) {
                this.invalidNameForm = true;
                return;
            }
            if (this.duracionForm.value <= 0) {
                this.invalidNameForm = true;
                return;
            }
            this.ejercicio.repeticiones = 0;
            this.ejercicio.series = 0;
            this.ejercicio.peso = 0;
            this.ejercicio.duracion = this.duracionForm.value;
        }
        this.dialogRef.close(this.ejercicio);
    }

    public selectEjercicio(ejercicio) {
        this.ejercicio = ejercicio;
    }

    private _filter(value: string): any[] {
        const filterValue = value.toLowerCase();
        const data: any = {};
        data.options = {
            "nombreAlimento": filterValue
        };
        return this.httpService.listarEjerciciosSinLoader(filterValue);
    }
}
