import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Observable  } from 'rxjs';
import { map, startWith, debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-md-agregar-profesor',
  templateUrl: './md-agregar-profesor.component.html',
  styleUrls: ['./md-agregar-profesor.component.scss'],
})
export class MdAgregarProfesorComponent implements OnInit {

  private profesorForm = new FormControl('', Validators.required);
  public filteredOptions: Observable<any[]>;
  public options: any[];
  public profesor;
  public mostrarError: boolean;

  constructor(
    public httpService: HttpService,
    public dialogRef: MatDialogRef<MdAgregarProfesorComponent>,
    public routeService: RoutesService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.filteredOptions = this.profesorForm.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        switchMap(value => {
          return this._filter(value);
        }),
      );
  }

  public selectProfesor(profesor) {
    this.profesor = profesor;
  }

  agregar() {
    this.mostrarError = false;
    if (!this.profesor || this.profesorForm.value !== this.profesor.usuarioId) {
      this.mostrarError = true;
      console.log(this.mostrarError);
      return;
    }
    this.dialogRef.close(this.profesor);
  }

  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    const data: any = {};
    data.options = {
      nombreAlimento: filterValue
    };
    if (this.profesorForm.value.length > 1) {
      return this.httpService.listarProfesoresSinLoader(filterValue);
    }
  }
}
