import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'agregar-profesor-dialog',
  templateUrl: './agregar-profesor.component.html',
})
export class AgregarProfesorDialog {

  constructor(
    public dialogRef: MatDialogRef<AgregarProfesorDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
