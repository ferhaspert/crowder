import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { UtilsService } from 'app/services/utils.service';
import { ModalService } from 'app/services/modal.service';
import { MdDetalleAlumnoComponent } from 'app/moduloDeportivo/md-detalle-alumno/md-detalle-alumno.component';
import { MdAsignarPlanComponent } from 'app/moduloDeportivo/md-asignar-plan/md-asignar-plan.component';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-alumnos-tab',
  templateUrl: './alumnos-tab.component.html',
  styleUrls: ['./alumnos-tab.component.scss'],
})
export class AlumnosTabComponent implements OnInit {

  public alumnos: any[];
  public solicitudes: any[];
  public user;

  constructor(
    private http: HttpService,
    private utils: UtilsService,
    private modalService: ModalService,
    private router: Router,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.alumnos = [];
    this.solicitudes = [];
    this.user = this.utils.loadFromLocalStorage('user');
    this.http.obtenerAlumnosActivos(this.user.id)
      .subscribe(data => {
        this.alumnos = data;
        console.log(data);
      });
    this.http.getSolicitudesPendientes(this.user.id)
      .subscribe(data2 => {
        this.solicitudes = data2;
      });
  }

  aceptarSolicitud(solicitud) {
    this.http.aceptarAlumno(solicitud.cliente.id, this.user.id)
      .subscribe(resultado => {
        if (resultado.result === 0) {
          this.modalService.openModalSuccess(resultado.message, {});
          this.quitarDeAlumnos(this.solicitudes.indexOf(solicitud));
          this.http.obtenerAlumnosActivos(this.user.id)
                .subscribe(data => {
                  this.alumnos = data;
                });
        } else {
          this.modalService.openModalError(resultado.message);
        }
        console.log(resultado);
      });
  }

  async gotoUserInfo(alumno) {
    this.router.navigate(['/md/tabs/userInfo'], {
      state: { example: alumno }
    });
  }

  async asignarPlanAlumno(alumno) {
    const modal = await this.modalCtrl.create({
      component: MdAsignarPlanComponent,
      componentProps: {
        alumno, // Es lo mismo que 'alumno': alumno
    }
    });
    return await modal.present();
  }

  rechazarSolicitud(solicitud) {
    this.http.rechazarAlumno(solicitud.cliente.id, this.user.id)
      .subscribe(resultado => {
        console.log(resultado);
        this.quitarDeAlumnos(this.solicitudes.indexOf(solicitud));
      });
  }

  quitarDeAlumnos(index) {
    if (index > -1) {
      this.solicitudes.splice(index, 1);
    }
  }
  
  eliminar(alumno){
    this.http.rechazarAlumno(alumno.id, this.user.id)
    .subscribe(resultado => {
      console.log(resultado);
      if(resultado.result == 0){
        let index = this.alumnos.indexOf(alumno);
        if (index > -1) {
          this.alumnos.splice(index, 1);
        }
      }
    });
  }

  async verEstadisticas(alumno){
    this.router.navigate(['/estadisticas'], {
      state: { id: alumno.id, nombre: alumno.nombre, apellido: alumno.apellido }
    });
  }
}
