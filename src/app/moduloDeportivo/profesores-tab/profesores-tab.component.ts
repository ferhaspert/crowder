import { Component, OnInit, Inject } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { UtilsService } from 'app/services/utils.service';
import { MatDialog } from '@angular/material/dialog';
import { MdAgregarProfesorComponent } from 'app/moduloDeportivo/md-agregar-profesor/md-agregar-profesor.component';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profesores-tab',
  templateUrl: './profesores-tab.component.html',
  styleUrls: ['./profesores-tab.component.scss'],
})
export class ProfesoresTabComponent implements OnInit {

  public profesores = [];

  constructor(
    public dialog: MatDialog,
    private httpService: HttpService,
    private routeService: RoutesService,
    private modalService: ModalService,
    private utils: UtilsService,
    public alertController: AlertController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.profesores = [];
    this.httpService.realizarGet(
      this.routeService.moduloDeportivo + 'getProfesoresActivos?clienteId=' + this.utils.loadFromLocalStorage('user').id)
      .subscribe(resultado => {
        this.profesores = resultado;
        // TODO abrir modal en caso de que no tenga planes activos
      });
  }

  abrirAgregarProfesorDialog() {
    const dialogRef = this.dialog.open(MdAgregarProfesorComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.httpService.realizarPost(
        this.routeService.moduloDeportivo + 'altaNuevoProfesor?clienteId=' + this.utils.loadFromLocalStorage('user').id +
        '&profUserId=' + result.usuarioId, {})
        .subscribe(resultado => {
          if (resultado.result === 0) {
             this.modalService.openModalSuccess('Solicitud de nuevo profesor enviada a ' + result.nombre + ' ' + result.apellido + '!',{});
          } else {
            this.modalService.openModalError(resultado.message);
          }
        });
    });
  }

  eliminarProfesor(profesor) {
    const fallbackTrue = () => {
      this.httpService.realizarPost(this.routeService.moduloDeportivo + 'bajaAlumno?clienteId=' +
        this.utils.loadFromLocalStorage('user').id + '&profUserId=' + profesor.id, {})
        .subscribe(result => {
          this.ngOnInit();
          this.mostrarOkMensaje('Profesor dado de baja correctamente.');
        });
    };
    this.presentAlertConfirm(fallbackTrue);

  }

  async gotoUserInfo(profesor) {
    this.router.navigate(['/md/tabs/profInfo'], {
      state: { example: profesor }
    });
  }
  
  async presentAlertConfirm(fallbackTrue, fallbackFalse = () => undefined) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: '¿Enviar solicitud?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            if (fallbackFalse) {
              fallbackFalse();
            }
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            if (fallbackTrue) {
              fallbackTrue();
            }

          }
        }
      ]
    });

    await alert.present();
  }

  async mostrarOkMensaje(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}

