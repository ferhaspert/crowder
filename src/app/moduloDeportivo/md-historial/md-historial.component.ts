import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { ModalService } from 'app/services/modal.service';
import { MatDialog } from '@angular/material';
import { DetallePlanDeportivoComponent } from 'app/modals/detalle-plan-deportivo/detalle-plan-deportivo.component';
import { UtilsService } from 'app/services/utils.service';
import { Router } from '@angular/router';
import { DeportivoMessageService } from 'app/services/message.service';

@Component({
  selector: 'app-md-historial',
  templateUrl: './md-historial.component.html',
  styleUrls: ['./md-historial.component.scss'],
})
export class MdHistorialComponent implements OnInit {

  public subscription;
  constructor(
    public dialog: MatDialog,
    private httpService: HttpService,
    private routeService: RoutesService,
    private modalService: ModalService,
    private utils: UtilsService,
    public router: Router,
    private messageService: DeportivoMessageService,
  ) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        if (message === 'reload') {
          this.ngOnInit();
        }
      }
    });
  }

  public planes = [];
  public user;

  ngOnInit() {
    this.planes = [];
    let service = "";
    this.user = this.utils.loadFromLocalStorage('user');

    if(this.user != null && this.user.rol == 2){
      service = "listarHistorialUsuario?clienteId=";
    }else if(this.user != null && this.user.rol == 3){
      service = "listarHistorialProfesor?profId=";
    }
    
    this.httpService.realizarGet(
      this.routeService.moduloDeportivo + service + JSON.parse(localStorage.getItem('user')).id)
      .subscribe(resultado => {
        this.planes = resultado;
        // this.planes.forEach(element => {
        //   element.planNutricional.dias.sort((a, b) => a.id - b.id);
        // });
        console.log(this.planes);
        // TODO abrir modal en caso de que no tenga planes activos
      }); 
  }

  verDetalle(plan): void {
    const dialogRef = this.dialog.open(DetallePlanDeportivoComponent, {
      data: plan.planDeportivo,
      panelClass: 'modal-detalle-plan'
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      });
  }

  estadoPlan(plan){
    return plan.estadoPlan == 1 ? "Activo" : "No Activo";
  }

  esCliente(){
    return (this.user.rol == 2);
  }

  eliminar(plan){
    let clienteId = this.user.id;
    let planId = plan.planDeportivo.id;

    this.httpService.realizarPost(this.routeService.moduloDeportivo + '/cambiarEstadoPlan?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=3' ,{})
      .subscribe(response => {
        if (response.result === 0) {      
          this.modalService.openModalSuccess(response.message,{});
          this.messageService.sendMessage('reload');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
  }

  desactivar(plan){
    let clienteId = this.user.id;
    let planId = plan.planDeportivo.id;
    if(plan.estadoPlan == 2){
      this.modalService.openModalInfo('El plan ya se encuentra inactivo.');
    }else{
      this.httpService.realizarPost(this.routeService.moduloDeportivo + '/cambiarEstadoPlan?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=2' ,{})
      .subscribe(response => {
        if (response.result === 0) {      
          this.modalService.openModalSuccess(response.message,{});
          this.messageService.sendMessage('reload');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
    }
  }

  reload() {
    this.router.navigateByUrl('md/tabs');
  }

  activar(plan){
    let clienteId = this.user.id;
    let planId = plan.planDeportivo.id;
    if(plan.estadoPlan == 1){
      this.modalService.openModalInfo('El plan ya se encuentra activo.');
    }else{
      this.httpService.realizarPost(this.routeService.moduloDeportivo + '/cambiarEstadoPlan?clienteId=' + clienteId + '&planId=' + planId + '&nuevoEstado=1' ,{})
      .subscribe(response => {
        if (response.result === 0) {      
          this.modalService.openModalSuccess(response.message, this.reload.bind(this));
          this.messageService.sendMessage('reload');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
    }
  }
}
