import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdEntrenamientoDiarioComponent } from './md-entrenamiento-diario.component';

describe('MdEntrenamientoDiarioComponent', () => {
  let component: MdEntrenamientoDiarioComponent;
  let fixture: ComponentFixture<MdEntrenamientoDiarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdEntrenamientoDiarioComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdEntrenamientoDiarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
