import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-md-detalle-profesor',
  templateUrl: './md-detalle-profesor.component.html',
  styleUrls: ['./md-detalle-profesor.component.scss'],
})
export class MdDetalleProfesorComponent implements OnInit {
  @Input() public profesor;
  public dialogRef: MatDialogRef<MdDetalleProfesorComponent>;
  public example;
  public dataLoaded;
  constructor(    
    public http: HttpService,
    public routeService: RoutesService,
    public router: Router,
    private modalCtrl: ModalController,
    )
    { 
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {example: string};
    this.profesor = state.example;
    }

  ngOnInit() {}

}
