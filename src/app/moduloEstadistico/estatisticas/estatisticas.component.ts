import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chart } from 'chart.js';
import { ChartsModule } from 'ng2-charts';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { DeportivoMessageService } from 'app/services/message.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-estatisticas',
  templateUrl: './estatisticas.component.html',
  styleUrls: ['./estatisticas.component.scss'],
})
export class EstatisticasComponent implements OnInit {

  public mySubscription: any;
  constructor(
    public http: HttpService,
    public routeService: RoutesService,
    public router: Router,
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        if(event.url == "/estadisticas") { this.ngOnInit() }
      }
    });
  }

  public estadisticas = {
    caloriasConsumidas: "",
    caloriasIngeridas: "",
    entrenamientosCumplidos: "",
    menuesCumplidos: "",
    caloriasConsumidasDiarias: "",
    caloriasIngeridasDiarias: ""
  }
  public semanas: number = 1;
  public datosCargados = false;
  public pedidosActuales = 0;
  public rol = JSON.parse(localStorage.getItem('user')).rol;
  public clienteId;
  public esProfesor = false;
  public alumnos: any[];
  public alumnoSelected = false;
  public alumno = {
    "id": "0",
    "nombre": "Seleccionar Alumno"
  }
  public periodoSeleccionado = {
    "semanas": 1,
    "value": "Semanal"
  }

  ngOnInit() {
    if (this.rol == 3) {
      this.esProfesor = true;
      this.http.obtenerAlumnosActivos(JSON.parse(localStorage.getItem('user')).id)
        .subscribe(data => {
          this.alumnos = data;
        });
      const navigation = this.router.getCurrentNavigation();
      const state = navigation.extras.state as { id: string, nombre: string, apellido: string };
      if (state != null) {
        this.selectAlumno(state);
      }
    }
    else {
      this.clienteId = JSON.parse(localStorage.getItem('user')).id;
      this.calcularEstadisticas()
      this.calcularCaloriasDiarias()
    }
  }

  calcularCaloriasDiarias() {
    this.pedidosActuales = this.pedidosActuales + 2;
    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getConsumoCaloricoPromedio?clienteId=' + this.clienteId + '&cantidadSemanas=' + 0)
      .subscribe(resultado => {
        console.log("Consumo calorico promedio = " + resultado)
        this.actualizarPedidos();
        if (resultado == null) {
          this.estadisticas.caloriasConsumidasDiarias = "0";
        }
        else {
          this.estadisticas.caloriasConsumidasDiarias = resultado;
        }
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getPromedioCaloriasIngeridas?clienteId=' + this.clienteId + '&cantidadSemanas=' + 0)
      .subscribe(resultado => {
        console.log("Promedio calorias ingeridas = " + resultado)
        this.actualizarPedidos();
        if (resultado == null) {
          this.estadisticas.caloriasIngeridasDiarias = "0";
        }
        else {
          this.estadisticas.caloriasIngeridasDiarias = resultado;
        }
      });

  }

  calcularEstadisticas() {
    this.pedidosActuales = this.pedidosActuales + 7;
    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getConsumoCaloricoPromedio?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Consumo calorico promedio = " + resultado)
        this.actualizarPedidos();
        if (resultado == null) {
          this.estadisticas.caloriasConsumidas = "0";
        }
        else {
          this.estadisticas.caloriasConsumidas = resultado;
        }
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getEntrenamientosCumplidos?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Entrenamientos Cumplidos = " + resultado)
        this.actualizarPedidos();
        this.estadisticas.entrenamientosCumplidos = resultado;
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getGruposMuscularesEntrenados?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Grupos musculares entrenados = " + resultado)
        this.actualizarPedidos();
        if (resultado != null && resultado.length > 0) {
          this.datasetGruposMusculares[0].data = resultado;
        }
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getPorcentajeDeTipoDeAlimentos?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Porcentaje De Tipo De Alimentos Mes = " + resultado)
        this.actualizarPedidos();
        if (resultado != null && (resultado[0] > 0 || resultado[1] > 0 || resultado[2] > 0)) {
          this.datasetNutrientes[0].data = resultado;
        }
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getTiposDeEntrenamiento?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Porcentaje tipo entrenamiento = " + resultado)
        this.actualizarPedidos();
        if (resultado != null) {
          this.datasetEjercicios[0].data = resultado;
        }
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getPorcentajeMenuesCumplidos?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Porcentaje de menues cumplidos = " + resultado)
        this.actualizarPedidos();
        this.estadisticas.menuesCumplidos = resultado;
      });

    this.http
      .realizarGet(this.routeService.moduloEstadistico + 'getPromedioCaloriasIngeridas?clienteId=' +
        this.clienteId + '&cantidadSemanas=' + this.semanas)
      .subscribe(resultado => {
        console.log("Promedio calorias ingeridas = " + resultado)
        this.actualizarPedidos();
        if (resultado == null) {
          this.estadisticas.caloriasIngeridas = "0";
        }
        else {
          this.estadisticas.caloriasIngeridas = resultado;
        }
      });
  }

  actualizarPedidos() {
    this.pedidosActuales = this.pedidosActuales - 1;
    if (this.pedidosActuales == 0) {
      this.datosCargados = true;
      console.log("Datos cargados!")
    }
  }

  cambiarPeriodo(semanas, value) {
    if (value != this.periodoSeleccionado.value) {
      this.semanas = semanas;
      this.calcularEstadisticas();
      this.periodoSeleccionado = {
        "semanas": semanas,
        "value": value
      }
    }
  }

  selectAlumno(alumno) {
    if (alumno.id != this.alumno.id) {
      this.alumnoSelected = true;
      this.clienteId = alumno.id;
      this.alumno = alumno;
      this.calcularEstadisticas();
    }
  }

  poseeAlumnos(){
    if(this.alumnos){
      return (this.alumnos.length > 0);
    }else{
      false;
    }
  }

  // Torso, Espalda, Piernas, Brazos, Core
  public chartTypeGruposMusculares = 'horizontalBar';
  public datasetGruposMusculares: Array<any> = [
    { data: [], label: '% de entrenamiento' },
  ];
  public labelsGruposMusculares: Array<any> = ['Torso', 'Espalda', 'Piernas', 'Brazos', 'Core'];
  public colorsGruposMusculares: Array<any> = [
    {
      backgroundColor: [
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
      ],
      borderColor: [
        '#f57c00',
        '#f57c00',
        '#f57c00',
        '#f57c00',
        '#f57c00',
      ],
      borderWidth: 2,
    },
  ];

  public chartOptions: any = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
        font: {
          size: 20,
        }
      }
    }
  };
  // Musculacion, Clase aerobica, running, natacion, otros
  public chartTypeEjercicios = 'horizontalBar';
  public datasetEjercicios: Array<any> = [
    { data: [], label: '% de ejercicio' },
  ];
  public labelsEjercicios: Array<any> = ['Musculación', 'Clase aeróbica', 'Running', 'Natación', 'Otros'];
  public colorsEjercicios: Array<any> = [
    {
      backgroundColor: [
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
        'rgba(245, 124, 0, 0.5)',
      ],
      borderColor: [
        '#f57c00',
        '#f57c00',
        '#f57c00',
        '#f57c00',
        '#f57c00',
        '#f57c00',
      ],
      borderWidth: 2,
    },
  ];
  // Grasas, Hidratos de carbono, Proteínas
  public chartTypeNutrientes = 'pie';
  public datasetNutrientes: Array<any> = [
    { data: [], label: '% de ejercicio' },
  ];
  public labelsNutrientes: Array<any> = ['Grasas', 'Hidratos de carbono', 'Proteínas'];
  public colorsNutrientes: Array<any> = [
    {
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(153, 102, 255, 0.2)',
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(153, 102, 255, 1)',
      ],
      borderWidth: 2,
    },
  ];

  public pieOptions: any = {
    responsive: true
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

}