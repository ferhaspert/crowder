import { FormControl } from '@angular/forms';

export function ValidateDate(date: FormControl): {[key: string]: any} | null {

  if ( new Date().getTime() - (new Date(date.value).getTime()) < (1000 * 60 * 60 * 24 * 365 * 18) ) {
    return { invalidDate: true };
  }
  return null;
}
