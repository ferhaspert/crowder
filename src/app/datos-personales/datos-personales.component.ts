import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Router } from '@angular/router';
import { UtilsService } from 'app/services/utils.service';
import { AlertController } from '@ionic/angular';
import { ModalService } from 'app/services/modal.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ValidateDate } from 'app/validators/date.validator';
import { Location } from '@angular/common';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.scss'],
})
export class DatosPersonalesComponent implements OnInit {
  public user;
  public nombre;
  public apellido;
  public mail;
  public fecha;
  private registroForm: FormGroup;

  constructor(    
    private httpService: HttpService,
    private routesService: RoutesService,
    private router: Router,
    private utils: UtilsService,
    private alertController: AlertController,
    private modalService:ModalService,
    private formBuilder: FormBuilder,
    private location: Location) {
      this.registroForm = this.formBuilder.group({
        nombre: ['', Validators.required],       
        apellido: ['', Validators.required],
        fecha: ['', [Validators.required, ValidateDate]],      
        mail: ['', Validators.required],
        //mail: ['', [Validators.pattern('[a-zA-Z0-9_-]+@[a-zA-Z0-9]+\.[a-zA-Z]{1,5}'), Validators.required]]       
     })
    }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.nombre = this.user.nombre;
    this.apellido = this.user.apellido;
    this.mail = this.user.mail;
    this.fecha = (this.user.fechaDeNacimiento);
  }
  
  actualizarDatos(){
    if (!this.registroForm.valid) {
      return;
    }
    const data = {
      id: this.user.id,
      nombre: this.nombre,
      apellido: this.apellido,
      mail: this.mail,
      fechaDeNacimiento: this.fecha,
    };
    console.log(data);
    this.httpService
      .realizarPost(this.routesService.moduloLogin + '/nutrischool/gestionUsuario/modificarDatosUsuario', data)
        .subscribe(response => {
          if (response.result === 0) {
            this.actualizarUser();
            this.modalService.openModalSuccess(response.message,{});
            this.router.navigateByUrl('dashboard');
          } else {
            this.modalService.openModalError(response.message);
          }
        console.log(response);
      });
  }
  actualizarUser(){
    this.httpService
      .realizarGet(this.routesService.moduloLogin + '/nutrischool/gestionUsuario/refreshUser?userId=' + this.user.id)
        .subscribe(response => {
          if (response != null) {
            this.user = response;
            this.utils.saveLocalStorage('user', this.user);
          }
      });
  }
  volver() {
    this.location.back();
  }

}
