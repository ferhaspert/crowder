import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './login/registro/registro.component';
import { RestablecerPassComponent } from './login/restablecer-pass/restablecer-pass.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { CiNutricionalesComponent } from './condicionesIniciales/ci-nutricionales/ci-nutricionales.component';
import { CiDeportivasComponent } from './condicionesIniciales/ci-deportivas/ci-deportivas.component';
import { MnTabsComponent } from './moduloNutricional/mn-tabs/mn-tabs.component';
import { MnCrearPlanVentanaComponent } from './moduloNutricional/mn-crear-plan/mn-crear-plan-ventana/mn-crear-plan-ventana.component';
import { MdPlanesAutogeneradosComponent } from './moduloDeportivo/md-crear-plan/md-planes-autogenerados/md-planes-autogenerados.component';
import { MdTabsComponent } from './moduloDeportivo/md-tabs/md-tabs.component';
import { MnCrearPlanGeneradoComponent } from './moduloNutricional/mn-crear-plan/mn-crear-plan-generado/mn-crear-plan-generado.component';
import { MdPlanesManualesComponent } from './moduloDeportivo/md-crear-plan/md-planes-manuales/md-planes-manuales.component';
import { MdDetalleAlumnoComponent } from './moduloDeportivo/md-detalle-alumno/md-detalle-alumno.component';
import { MdDetalleProfesorComponent } from './moduloDeportivo/md-detalle-profesor/md-detalle-profesor.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { SugerenciasComponent } from './sugerencias/sugerencias.component';
import { EstatisticasComponent } from './moduloEstadistico/estatisticas/estatisticas.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'login', component: LoginComponent },
  { path: 'login/registro', component: RegistroComponent},
  { path: 'login/restablecerContrasena', component:  RestablecerPassComponent},
  { path: 'dashboard', component:  DashboardComponent},
  { path: 'condicionesIniciales/nutricionales', component:  CiNutricionalesComponent},
  { path: 'condicionesIniciales/deportivas', component:  CiDeportivasComponent},
  { path: 'mn/tabs', component:  MnTabsComponent},
  { path: 'mn/nuevoPlan/manual', component: MnCrearPlanVentanaComponent},
  { path: 'mn/nuevoPlan/autoGenerado', component: MnCrearPlanGeneradoComponent},
  { path: 'md/nuevoPlan/autoGenerado', component: MdPlanesAutogeneradosComponent},
  { path: 'md/nuevoPlan/manual', component: MdPlanesManualesComponent},
  { path: 'md/tabs', component: MdTabsComponent},
  { path: 'md/tabs/userInfo', component: MdDetalleAlumnoComponent},
  { path: 'md/tabs/profInfo', component: MdDetalleProfesorComponent},
  { path: 'datosPersonales', component: DatosPersonalesComponent},
  { path: 'sugerencias', component: SugerenciasComponent},
  { path: 'estadisticas', component: EstatisticasComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
