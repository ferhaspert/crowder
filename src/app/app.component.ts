import { Component, OnInit } from '@angular/core';
import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilsService } from 'app/services/utils.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  private user;
  public mySubscription: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private router: Router,
    private utils: UtilsService,
  ) {
    this.initializeApp();
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        if(event.url == "/dashboard") { this.ngOnInit() }
      }
    });
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  toggleMenu() {
    this.menu.toggle(); //Add this method to your button click function
  }

  cerrarSesion() {
    this.utils.saveLocalStorage('user', {});
    this.router.navigateByUrl('login');
  }

  esCliente(){
    if (this.user != null) {
      return (this.user.rol == 2)
    } 
    return false;
  }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    console.log(this.user);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
