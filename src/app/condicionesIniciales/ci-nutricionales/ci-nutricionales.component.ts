import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UtilsService } from 'app/services/utils.service';
import { AlertController } from '@ionic/angular';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-ci-nutricionales',
  templateUrl: './ci-nutricionales.component.html',
  styleUrls: ['./ci-nutricionales.component.scss'],
})

export class CiNutricionalesComponent implements OnInit {

  public altura: number;
  public peso: number;
  public condicionesNutricionales = [
    {id: 1, descripcion: 'Celíaco', checked: false},
    {id: 2, descripcion: 'Hipertenso', checked: false},
    {id: 3, descripcion: 'Diabético', checked: false},
    {id: 9, descripcion: 'Bajo colesterol', checked: false},
    {id: 4, descripcion: 'Vegetariano', checked: false},
    {id: 5, descripcion: 'Vegano', checked: false},
    {id: 8, descripcion: 'Disminución de peso', checked: false},
    {id: 10, descripcion: 'Control de peso', checked: false},
    {id: 11, descripcion: 'Aumento de Peso', checked: false},
    {id: 12, descripcion: 'Onmivoro', checked: false},
  ];

  public objetivos = [
    {id: 8, descripcion: 'Disminución de peso', checked: false},
    {id: 10, descripcion: 'Control de peso', checked: false},
    {id: 11, descripcion: 'Aumento de Peso', checked: false},
  ];

  public alimenticias = [
    {id: 12, descripcion: 'Onmivoro', checked: false},
    {id: 4, descripcion: 'Vegetariano', checked: false},
    {id: 5, descripcion: 'Vegano', checked: false},
  ];

  public patologias = [
    {id: 1, descripcion: 'Celíaco', checked: false},
    {id: 2, descripcion: 'Hipertenso', checked: false},
    {id: 3, descripcion: 'Diabético', checked: false},
    {id: 9, descripcion: 'Bajo colesterol', checked: false},
  ];

  public selectedArray: any[];
  public user;
  public objetivo;
  public alimentacion;

  constructor(
    private httpService: HttpService,
    private routesService: RoutesService,
    private router: Router,
    private location: Location,
    private utils: UtilsService,
    private alertController: AlertController,
    private modalService:ModalService,
  ) { }

  ngOnInit() {
    this.user = this.utils.loadFromLocalStorage('user');
    this.altura = this.user.altura;
    this.peso = this.user.peso;
    if(this.user.condiciones){
      for (const condicion of this.user.condiciones){
        if(this.alimenticias.find(c => c.id == condicion.condicion.id)){
          this.alimentacion = condicion.condicion.id;
        }else if(this.objetivos.find(c => c.id == condicion.condicion.id)){
          this.objetivo = condicion.condicion.id;
        }else if(this.patologias.find(c => c.id == condicion.condicion.id)){
          this.patologias.find(c => c.id == condicion.condicion.id).checked = true;
        }
      }
    }
  }

  esAlimenticia(condicion){
    return this.alimenticias.includes(condicion);
  }

  cambiarObjetivo(val){
    this.objetivo = val;
  }

  cambiarAlimentacion(val){
    this.alimentacion = val;
  }

  armarCondiciones2(){
    const array = [];
    this.patologias.forEach(element => {
      if(element.checked){
        array.push(element.id);
      }
    });
    if(this.objetivo != null){
      array.push(this.objetivo);
    }
    if(this.alimentacion != null){
      array.push(this.alimentacion);
    }
    return array;
  }

  armarCondiciones() {
    const array = [];
    this.condicionesNutricionales.forEach(element => {
      if (element.checked) {
        array.push(element.id);
      }
    });
    return array;
  }

  setLocalStorage(data: any) {
    this.user.peso = data.peso;
    this.user.altura = data.altura;
    const nuevasCondiciones = [];
    for (const nuevaCondicion of data.condicionesNutricionales) {
      const condicion = {condicion: this.condicionesNutricionales.filter((c) => c.id === nuevaCondicion)[0]};
      nuevasCondiciones.push(condicion);
    }
    console.log(nuevasCondiciones);
    this.user.condiciones = nuevasCondiciones;
    console.log(this.user.condiciones);
    this.utils.saveLocalStorage('user', this.user);
  }

  enviar() {
    const data = {
      clienteId: JSON.parse(localStorage.getItem('user')).id,
      altura: this.altura,
      peso: this.peso,
      condicionesNutricionales: this.armarCondiciones2()
    };
    if ( !data.condicionesNutricionales.length ) {
      this.presentAlertConfirm('Debe seleccionar por lo menos una condición');
      return;
    }
    if (!this.alimentacion) {
      this.presentAlertConfirm('Debe seleccionar un tipo de alimentación');
      return;
    }
    if (!this.objetivo) {
      this.presentAlertConfirm('Debe seleccionar un objetivo');
      return;
    }
    console.log(data);
    this.httpService.realizarPost(this.routesService.moduloNutricional + '/nutrischool/gestionNutricional/guardarCondicionesIniciales', data)
      .subscribe(response => {
        if (response.result === 0) {
          this.setLocalStorage(data);
          this.modalService.openModalSuccess(response.message, this.navigateDashboard.bind(this));
        } else {
          this.modalService.openModalError(response.message);
        }
      });
  }

  navigateDashboard() {
    this.router.navigateByUrl('dashboard');
  }

  volver() {
    this.router.navigateByUrl('dashboard');
  }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
        }
      ]
    });

    await alert.present();
  }
}
