import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import { RoutesService } from 'app/services/routes.service';
import { MatDialog } from '@angular/material';
import { UtilsService } from 'app/services/utils.service';

@Component({
  selector: 'app-sugerencias',
  templateUrl: './sugerencias.component.html',
  styleUrls: ['./sugerencias.component.scss'],
})
export class SugerenciasComponent implements OnInit {
  public sugerencias: any[] = [];
  public user;
  constructor(public http: HttpService, public routeService: RoutesService, public dialog: MatDialog, private utils: UtilsService,) { }

  ngOnInit() {
    let pageNumber = 0;
    //let pageNumber = Math.floor((Math.random() * 2) + 1);
    // this.http
    //   .realizarGet(this.routeService.moduloDeportivo + "getSugerenciasDeportivas?pageNumber=" + pageNumber)
    //     .subscribe(resultado => {
    //       this.sugerenciasDeportivas = resultado;
    //       console.log(resultado)
    //   });
      this.http
      .realizarGet(this.routeService.moduloNutricional + "/nutrischool/gestionNutricional/getSugerenciasNutricionales?pageNumber=" + pageNumber)
        .subscribe(resultado => {
          this.sugerencias = resultado;
          console.log(resultado)
      });
  }

}
