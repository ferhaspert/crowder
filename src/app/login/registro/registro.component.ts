import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { HttpService } from '../../services/http.service';
import { Router, RouterModule } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ValidateDate } from '../../validators/date.validator';
import { ModalService } from 'app/services/modal.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss'],
})

export class RegistroComponent implements OnInit {

  private nombre: string;
  private apellido: string;
  private idUsuario: string;
  private mail: string;
  private password: string;
  private rol: number;
  private fecha: string = new Date(1990, 1, 1).toISOString();
  private sexo: string;
  private registroForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private router: Router,
    public alertController: AlertController,
    private formBuilder: FormBuilder,
    private modalService:ModalService,
    private location:Location,
  ) {
    this.registroForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      sexo: ['', Validators.required],
      apellido: ['', Validators.required],
      fecha: ['', [Validators.required, ValidateDate]],
      idUsuario: ['', Validators.required],
      // mail: ['', [Validators.pattern('[a-zA-Z0-9_-]+@[a-zA-Z0-9]+\.[a-zA-Z]{1,5}'), Validators.required]],
      mail: ['', Validators.required],
      rol: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {}

  cambioFecha(newObj) {
    console.log('cambio');
  }

  createUser() {
    console.log(this.registroForm.get('mail'));
    if (!this.registroForm.valid) {
      return;
    }
    const data = {
      nombre: this.nombre,
      apellido: this.apellido,
      idUsuario: this.idUsuario,
      mail: this.mail,
      password: this.password,
      rol: this.rol,
      fechaDeNacimiento: this.fecha,
      sexo: this.sexo,
      altura: 170,
      peso: 80,
      profesor: false,
    };
    console.log(data);

    if(this.rol == 3){
      data.profesor = true;
    }
    
    this.httpService.createUser(data)
      .subscribe(response => {
        if (response.result === 0) {
          this.modalService.openModalSuccess(response.message,this.router.navigateByUrl('login'));
          this.router.navigateByUrl('login');
        } else {
          this.modalService.openModalError(response.message);
        }
        console.log(response);
      });
  }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  volver() {
    this.location.back();
  }
  navigateContrasena(){
    this.router.navigateByUrl("login/restablecerContrasena")
  }
}
