import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { UtilsService } from '../services/utils.service';
import { LoginResponse } from '../interfaces/login/login-response';
import { Router, RouterModule } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  private idUsuario: string;
  private password: string;
  private loginForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private router: Router,
    public alertController: AlertController,
    private formBuilder: FormBuilder,
    public utils: UtilsService,
    private modalService: ModalService,
  ) {
    this.loginForm = this.formBuilder.group({
    idUsuario: ['', Validators.required],
    password: ['', Validators.required],
    });
  }
  ngOnInit() {}

  login() {
    this.httpService.tryLogin(
      { idUsuario : this.idUsuario, password : this.password})
      .subscribe(data => {
        console.log(data);
        if ( data.result === 0 ) {
          this.router.navigateByUrl('dashboard');
          this.utils.saveLocalStorage('user', data.usuarioAplicacion);
          this.utils.saveLocalStorage('token', data.token)
        } else if(data.result == 6){
          this.modalService.openModalInfo(data.message);
        } else if (data.result !== 0) {
          this.modalService.openModalError(data.message);
        }
      });
  }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  createUser() {
      this.router.navigateByUrl('login/registro');
  }

  forgotPassword() {
      this.router.navigateByUrl('login/restablecerContrasena');
  }
}
