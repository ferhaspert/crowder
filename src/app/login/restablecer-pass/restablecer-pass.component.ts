import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { AlertController } from '@ionic/angular';
import { Router, RouterModule } from '@angular/router';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import { Location } from '@angular/common';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-restablecer-pass',
  templateUrl: './restablecer-pass.component.html',
  styleUrls: ['./restablecer-pass.component.scss'],
})

export class RestablecerPassComponent implements OnInit {

  private userId: string;
  private mail: string;
  private passwordNueva: string;
  private restablecerPassForm: FormGroup;

  constructor(
    private httpService: HttpService,
    public alertController: AlertController,
    public router: Router,
    private location: Location,
    private formBuilder: FormBuilder,
    private modalService: ModalService,
  ) {
    this.restablecerPassForm = formBuilder.group({
      userId: ['', Validators.required],
      mail: ['', Validators.required],
      passwordNueva: ['', Validators.required],
    });
  }

  ngOnInit() {}

  resetPassword() {
    if (!this.restablecerPassForm.valid) {
      console.log(this.restablecerPassForm);
      return;
    }
    const data = {
      userId: this.userId,
      mail: this.mail,
      passwordNueva: this.passwordNueva
    };

    console.log(data);
    this.httpService.resetPassword(data)
      .subscribe(response => {
        if (response.result === 0) {
          this.modalService.openModalSuccess(response.message,{});
          this.router.navigateByUrl('login');
        } else {
          this.modalService.openModalError(response.message);
        }
      });
    }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
         {
          text: 'Ok',
          }
      ]
    });
    await alert.present();
  }

  volver() {
    this.location.back();
  }
}
