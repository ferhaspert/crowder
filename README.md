# NutriSchool_FrontEnd

UTN - Proyecto Final 2019 - NutriSchool - FrontEnd

Como instalar las dependencias necesarias para levantar el front end

Es conveniento usar git bash, si no lo tienen estaria bueno instalarlo

1. Instalar Node y npm -> https://nodejs.org/es/
	para ver si quedo bien instalado correr (deberia devolver las verciones de cada uno):
	
	"npm -v"
	"node -v"
	
2. Descargar instalar JDK si no tienen -> https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
	Pueden ver su version de java corriendo "java -version"

3. Setear la variable de entorno JAVA_HOME como se explica aca -> https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#java-development-kit-jdk
	
4. Instalar cordova ejecutando "npm install -g cordova"
	Para ver si quedo instalado correr "cordova -v"

5. Descargar e instalar Android Studio y la sdk (herramientas para compilar las versiones). Hay que agregar en las variables de entorno del sistema la ruta de la sdk. 
	La ruta de la sdk, pueden verla en el Android Studio. Cuando lo abren, a la derecha arriba hay una lupa y buscar "sdk manager". 
	
6. Dentro de android Studio, buscar sdk manager, abrir esa opcion y entrar a SDK Tools. Tildar las opciones Google USB Driver, Google Web Driver, Androdi Support Repository, 
	Android SDK Build Tools, 
7. Descargar Gradle y setear la variable de entorno como se explica aca -> https://gradle.org/install/#manually

8. Setear las variables de entorno de ANDROID_HOME Y agregar a path el directorio de la SDK como explica aca -> https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#setting-environment-variables

9. Instalar Angular npm i -g @angular/cli

10. npm install -g ionic